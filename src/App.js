import React from "react";
import { Root } from "native-base";
import { StackNavigator, DrawerNavigator } from "react-navigation";

import Landing from "./screens/landing";
import Home from "./screens/home";
import SideBar from "./screens/sidebar";

import Login from "./screens/login";
import Register from "./screens/register";
import OTP from "./screens/register/otp";
import Documents from "./screens/register/upload_documents";
import ForgotPassword from "./screens/forgot_password";
import SetPassword from "./screens/setpassword";

import ProArea from "./screens/proarea";
import ProAreaSingle from "./screens/proarea_request"

import Profile from "./screens/profile";
import Notifications from "./screens/notifications";
import SingleNotification from "./screens/notifications/single_notification";
import Payments from "./screens/payments";
import ChangePassword from "./screens/change_password";

import Legal from "./screens/legal";
import LegalContent from "./screens/legal/legal_content";

import UpdateProfile from "./screens/profile/update_profile";
import UpdateAbout from "./screens/profile/update_about";
import UpdateEducation from "./screens/profile/update_education";
import UpdateExperience from "./screens/profile/update_experience";
import UploadPicture from "./screens/profile/upload_picture";

const Drawer = DrawerNavigator({
  Home: { screen: Home },
  Login: { screen: Login },
  Register: { screen: Register },
  OTP: { screen: OTP },
  Documents: { screen: Documents },
  ForgotPassword: { screen: ForgotPassword },
  SetPassword: { screen: SetPassword },
  ProArea: { screen: ProArea },
  ProAreaSingle: { screen: ProAreaSingle },
  Profile: { screen: Profile },
  Notifications: { screen: Notifications },
  SingleNotification: { screen: SingleNotification },
  Payments: { screen: Payments },
  ChangePassword: { screen: ChangePassword },
  Legal: { screen: Legal },
  LegalContent: { screen: LegalContent },
  Landing: { screen: Landing },
  UpdateProfile: { screen: UpdateProfile },
  UpdateAbout: { screen: UpdateAbout },
  UpdateEducation: { screen: UpdateEducation },
  UpdateExperience: { screen: UpdateExperience },
  UploadPicture: { screen: UploadPicture },
},
{
    initialRouteName: "Landing",
    contentOptions: {
      activeTintColor: "#e91e63"
    },
    contentComponent: props => <SideBar {...props} />
  }
);


const AppNavigator = StackNavigator(
  {
    Drawer: { screen: Drawer },

    Landing: { screen: Landing },
    Login: { screen: Login },
    Register: { screen: Register },
    OTP: { screen: OTP },
    Documents: { screen: Documents },

    ForgotPassword: { screen: ForgotPassword },
    SetPassword: { screen: SetPassword },
    ChangePassword: { screen: ChangePassword },

    ProArea: { screen: ProArea },
    ProAreaSingle: { screen: ProAreaSingle },

    Profile: { screen: Profile },
    UpdateProfile: { screen: UpdateProfile },
    UpdateAbout: { screen: UpdateAbout },
    UpdateEducation: { screen: UpdateEducation },
    UpdateExperience: { screen: UpdateExperience },
    UploadPicture: { screen: UploadPicture },

    Notifications: { screen: Notifications },
    SingleNotification: { screen: SingleNotification },
    Payments: { screen: Payments },

    Legal: { screen: Legal },
    LegalContent: { screen: LegalContent },

  },
  {
  initialRouteName: "Drawer",
  headerMode: "none"
  }
);

export default () =>
  <Root>
    <AppNavigator />
  </Root>;
