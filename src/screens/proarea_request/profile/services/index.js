import React, { Component } from "react";
import { Content,ListItem,Text,Left,Right,Body } from "native-base";

import c_styles from "AjiraPro/src/screens/common_styles";

export default class Services extends Component {

  constructor(props){
    super(props);

    this.state = {
        quoteDetails: ""
      }
   }

  componentDidMount () {

    this.setState({
      quoteDetails: this.props.screenProps.quoteData
    })
  }

  render() {
    return (
      <Content>
          <ListItem itemDivider>
            <Left>
              <Text>Service:</Text>
            </Left>
            <Body/>
            <Right/>
          </ListItem>
          <ListItem>
            <Text>{this.state.quoteDetails.service}</Text>
          </ListItem>

          <ListItem itemDivider>
            <Left>
              <Text>Services Details</Text>
            </Left>
            <Body/>
            <Right/>
          </ListItem>

          <ListItem>
            <Text>Number of Services Requested:</Text>
            <Body><Text style={c_styles.textTheme}>{this.state.quoteDetails.noOfServicesRequested}</Text></Body>
          </ListItem>

          <ListItem last>
            <Text>Number of Services Completed:</Text>
            <Body><Text style={c_styles.textTheme}>{this.state.quoteDetails.noOfServicesCompleted}</Text></Body>
          </ListItem>
      </Content>
    );
  }
}
