import React, { Component } from "react";
import { Content,Button,ListItem,Text,Left,Right,Body } from "native-base";

import c_styles from "AjiraPro/src/screens/common_styles";

export default class Info extends Component {

  constructor(props){
    super(props);

    this.state = {
        location: "",
        quoteDetails: ""
      }
   }

  componentDidMount () {

    this.setState({
      location: this.props.screenProps.location,
      quoteDetails: this.props.screenProps.quoteData
    })
  }
  render() {
      return (
          <Content>
              <ListItem itemDivider>
                <Left>
                  <Text>Personal</Text>
                </Left>
                <Body/>
                <Right/>
              </ListItem>
              <ListItem>
                <Text>Email:</Text>
                <Body><Text style={c_styles.textTheme}>{this.state.quoteDetails.email}</Text></Body>
              </ListItem>
              <ListItem>
                <Text>Phone Number:</Text>
                <Body><Text style={c_styles.textTheme}>+254{this.state.quoteDetails.mobileNo}</Text></Body>
              </ListItem>
              <ListItem>
                <Text>Webiste:</Text>
                <Body><Text style={c_styles.textTheme}>{this.state.quoteDetails.website}</Text></Body>
              </ListItem>
              <ListItem last>
                <Text>Location:</Text>
                <Body><Text style={c_styles.textTheme}>{this.state.location}</Text></Body>
              </ListItem>
          </Content>
      );
    }
}
