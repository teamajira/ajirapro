const React = require("react-native");
const { StyleSheet, Dimensions, Platform } = React;
const deviceHeight = Dimensions.get("window").height;

export default {
  imageHolder:{
    backgroundColor: "#FFF",
    justifyContent: 'center',
    alignItems: "center",
    marginTop: 50
  },
  profileImage: {
    width: 200,
    height: 200,
    borderRadius: 200/2
  },
  profileDetails: {
    textAlign:"center",
    justifyContent: "center",
    marginLeft: -10
  },
  profileinfoIcons: {
    color:"#FF901E",
    fontSize:30,
  },
  listcontainer: {
    flex: 1,
    flexDirection: 'row',
    borderBottomColor: '#EEE',
    borderBottomWidth: 1,
    marginBottom: 30,
  },
  listfirstwidth: {
    width: '40%',
    alignSelf: 'auto',
  },
  listsecondwidth: {
    width: '60%',
  }
 };
