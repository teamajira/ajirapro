import React, { Component } from "react";
import {AsyncStorage,NetInfo,Image,View } from "react-native";
import {Container,Header,Title,Button,Icon,Tabs,Tab,TabHeading,Right,Left,Body,ScrollableTab,Badge,Text,Thumbnail,Content,H2} from "native-base";

/*Import required tabs*/
import Info from "./info";
import Services from "./services";
import Rating from "./rating";

/*Import formData*/
import FormData from 'FormData';
import CryptoJS from 'crypto-js';
import DeviceInfo from 'react-native-device-info';
import DialogProgress from 'react-native-dialog-progress';

/*Import specific styles for profile page*/
import styles from "./styles";

/*Import required API related functions*/

let sha1 = require('sha1');

/*Spinner dialog options*/
const options = {
    title:"Loading Data",
    message:"Please wait...",
    isCancelable:true
}

const deviceId = DeviceInfo.getUniqueID();
const osVersion = DeviceInfo.getVersion();

import c_styles from "AjiraPro/src/screens/common_styles";

const tabsUnder = {
  tabBarUnderlineStyle: { backgroundColor: '#9CDEE6' }
}

let customerName = null;


export default class Profile extends Component {

  constructor(props){
    super(props);

    this.state = {
        professionaltopInfo:"",
        professionalpersonalInfo: "",
        professionalAbout: "",
        token: ""
      }
   }

   _toUpper(str) {

     if(str){
       return str .toLowerCase()
       .split(' ')
       .map(function(word) {
         return word[0].toUpperCase() + word.substr(1);
       })
       .join(' ');
     }
   }


  render() {

    let quoteDetails = this.props.screenProps.quoteData;
    let requestDetails = this.props.screenProps.requestData;
    var data = JSON.stringify(quoteDetails)

    customerName = quoteDetails.custProfile.topInfo.firstName+" "+quoteDetails.custProfile.topInfo.lastName

    return (
      <Container >
          <Text></Text>
          <Content>
            <View>
              <View  style={styles.imageHolder}>
                  <Thumbnail large source={{uri:this.state.professionaltopInfo.personalPhoto}} style={styles.profileImage} />
              </View>
              <View>
                <Text style={{textAlign:"center", marginTop:25}}>
                <H2>{this._toUpper(customerName)}</H2>
                </Text>
              </View>
            </View>

            <Tabs {... tabsUnder}  renderTabBar={() => <ScrollableTab style={{backgroundColor: "transparent"}} />}  >
              <Tab heading={
                <TabHeading style={{backgroundColor: "transparent"}}>
                    <Text style={c_styles.textTheme}>Info</Text>
                </TabHeading>}>
                <Info screenProps={{ quoteData: quoteDetails.custProfile.personalInfo , location:requestDetails.location }}  />
              </Tab>

              <Tab heading={
                <TabHeading style={{backgroundColor: "transparent"}} >
                    <Text style={c_styles.textTheme}>Services</Text>
                </TabHeading>}>
                <Services screenProps={{ quoteData: quoteDetails.custProfile.services }} />
              </Tab>

              <Tab heading={
                <TabHeading  style={{backgroundColor: "transparent"}}>
                    <Text style={c_styles.textTheme}>Rating</Text>
                </TabHeading>}>
                <Rating screenProps={{ quoteData: quoteDetails.custProfile.topInfo }} />
              </Tab>
            </Tabs>
          </Content>

      </Container>
    );
  }
}
