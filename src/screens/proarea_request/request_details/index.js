import React, { Component } from "react";
import Icon from 'react-native-vector-icons/FontAwesome';
import MapView, { PROVIDER_GOOGLE, Marker  } from 'react-native-maps'
import { View,FlatList,Platform,PermissionsAndroid,Alert,Modal } from "react-native";
import { Container, Content, Header, Title, Button, Text, Body, Left, Right, ListItem } from "native-base";

import styles from "./styles";
import { COLOR_TURGUOISE } from "AjiraPro/src/screens/common_scripts";

export default class RequestDetails extends Component {

  constructor() {
    super();
    this.state = {
      mapModalVisible: false,
     };
  }

  setMapModalVisible(visible) {
    this.setState({mapModalVisible: visible});
  }

  render() {

    let quoteDetails = this.props.screenProps.quoteDetails;
    var data = JSON.stringify(quoteDetails)
    var LATITUDE = parseFloat(quoteDetails.latitude);
    var LONGITUDE = parseFloat(quoteDetails.longitude);
    //console.log("Request Details "+ JSON.stringify(quoteDetails));

    return (
      <Container>

      {/*User Map*/}
      <Modal
        animationType="slide"
        transparent={false}
        visible={this.state.mapModalVisible}
        onRequestClose={() => {
          Alert.alert('Close Map.');
        }}>
        <Container>
          <Header androidStatusBarColor="#9CDEE6" style={styles.modalHeader}>
          <Left/>
          <Body>
            <Title>Customer Location</Title>
          </Body>
            <Right>
              <Button transparent
                onPress={() => {this.setMapModalVisible(!this.state.mapModalVisible);}}>
                <Icon name="close" color="#fff" size={20} />
              </Button>
            </Right>
          </Header>

          <Content padder>
            <MapView
            provider={PROVIDER_GOOGLE}
            style={{flex: 1, width:"100%", height: 500}}
            region={{
              latitude: LATITUDE ,
              longitude: LONGITUDE ,
              latitudeDelta: 0.0922,
              longitudeDelta: 0.0421
            }}
            showsUserLocation>
               <Marker
                 coordinate={{
                   latitude: LATITUDE,
                   longitude: LONGITUDE,
                 }}
                 title={quoteDetails.venue}
               />
            </MapView>
          </Content>
        </Container>
      </Modal>


      <Content padder>
        <Text>Quote Questions and
          <Text> </Text>
          <Text onPress={() => { this.setMapModalVisible(true);}}>
            <Text style={styles.textLocation}>Customer Location</Text>
            <Icon name="map-pin" color={COLOR_TURGUOISE} size={30} />
          </Text>
        </Text>

        <FlatList
            data={quoteDetails.questions}
            keyExtractor = { (item, index) => index.toString() }
            renderItem={({item}) =>
              <ListItem transparent >
                <Body>
                  <Text style={styles.textQuestion}>
                    {item.question}
                  </Text>
                  <Text numberOfLines={1} style={styles.textAnswer} >
                    {item.answer}
                  </Text>
                </Body>
              </ListItem>}
            />
      </Content>
      </Container>
    );
  }
}
