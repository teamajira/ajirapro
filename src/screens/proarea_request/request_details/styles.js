import { COLOR_TURGUOISE,COLOR_ORANGE,COLOR_GRAY } from "AjiraPro/src/screens/common_scripts";

export default {
  textQuestion: {
    fontSize: 16,
    fontFamily: "Thasadith-Bold"
  },
  textAnswer: {
    color: COLOR_TURGUOISE,
    fontSize: 14
  },
  textLocation: {
    color: COLOR_TURGUOISE,
    textDecorationLine: 'underline'
  },
  modalHeader:{
    backgroundColor: COLOR_TURGUOISE
  }


}
