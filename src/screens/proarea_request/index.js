import React, { Component } from "react";
import {AsyncStorage,NetInfo, View } from "react-native";
import {
  Container,
  Header,
  Title,
  Button,
  Icon,
  Tabs,
  Tab,
  TabHeading,
  Right,
  Left,
  Body,
  ScrollableTab,
  Badge,
  Text
} from "native-base";

import DialogProgress from 'react-native-dialog-progress';
import Quote from "./quote";
import Profile from "./profile";
import RequestDetails from "./request_details";
import c_styles from "AjiraPro/src/screens/common_styles";

var jobID = null;
var quoteDetails = null;
var detailQuote = null;
var requestData = null;

class ProAreaSingle extends Component {

  constructor(props){

    super(props);

    this.state ={ isLoading: true};
    var { params } = this.props.navigation.state;
    quoteDetails = params ? params.quoteData : null
    detailQuote = params ? params.detailData : null
    requestData = params ? params.requestData : null

    jobID = quoteDetails.jobId

    //console.log("Quote Request Details "+ JSON.stringify(requestData));
  }

  render() {
    return (
      <Container >
        <Header hasTabs androidStatusBarColor="#9CDEE6" style={c_styles.bgColor}>
          <Left>
          <Button
            transparent
            onPress={() => this.props.navigation.navigate("ProArea")}>
            <Icon name="arrow-back" />
          </Button>
          </Left>
          <Body>
            <Title>Job ID:{jobID}</Title>
          </Body>
          <Right>
            <Button transparent onPress={() => this.props.navigation.navigate('Notifications')}>
              <Icon style={{ fontSize: 35 }} name="notifications" />
              <Badge style={{ position: 'absolute',marginLeft: 30 }}><Text>2</Text></Badge>
            </Button>
          </Right>
        </Header>

        <Tabs renderTabBar={() => <ScrollableTab style={c_styles.bgColor} />}  >

          <Tab heading={
            <TabHeading style={c_styles.bgColor}>
                <Text style={c_styles.text}>Quote</Text>
            </TabHeading>}>
            <Quote screenProps={{ quoteData: quoteDetails , requestData:requestData }} />
          </Tab>

          <Tab heading={
            <TabHeading style={c_styles.bgColor}>
                <Text style={c_styles.text}>Request Details</Text>
            </TabHeading>}>
            <RequestDetails screenProps={{ quoteDetails: detailQuote}} />
          </Tab>

          <Tab heading={
            <TabHeading style={c_styles.bgColor}>
                <Text style={c_styles.text}>Profile</Text>
            </TabHeading>}>
            <Profile screenProps={{ quoteData: quoteDetails , requestData:requestData }}  />
          </Tab>

        </Tabs>
      </Container>
    );
  }
}

export default ProAreaSingle;
