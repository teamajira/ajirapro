import React, { Component } from "react";
import moment from 'moment';
import StarRating from 'react-native-star-rating';
import Icon from 'react-native-vector-icons/FontAwesome';
import { ActivityIndicator,AsyncStorage,NetInfo,View,Alert,Image,Linking,Modal,Platform } from "react-native";
import { Container, Header,Content, Title, Label, Text, Body, Left, Right, Button, Textarea, Picker, Item, Input, Form, Card, CardItem, Toast } from "native-base";

import FormData from 'FormData';
import DeviceInfo from 'react-native-device-info';
import DialogProgress from 'react-native-dialog-progress';
import { Grid, Col } from "react-native-easy-grid";

import styles from "./styles";
import c_styles from "AjiraPro/src/screens/common_styles";
const completedImage = require("AjiraPro/assets/completed.png");
import { COLOR_TURGUOISE,COLOR_ORANGE,FORMAT_NUMBER,HASH,API_URL,APP_KEY,APP_VERSION, API_START_SERVICE,API_COMPLETE_SERVICE,API_RATE_CUSTOMER }  from "AjiraPro/src/screens/common_scripts";

let sha1 = require('sha1');
const options = {
    title:"Loading Data",
    message:"Please wait...",
    isCancelable:true
}

const deviceId = DeviceInfo.getUniqueID();
const osVersion = DeviceInfo.getVersion();

let hiredData = null;
let statusDisplay = null;
let messageDisplay = null;
let calendarStart = null;
let calendarEnd = null;
let buttonHired = null;
let quoteSubtotal = null;

export default class Quote extends Component {

  constructor(props){
    super(props);

    this.state = {
        jsonData: "",
        price: "",
        starCount: 1,
        ratingMessage: "",
        token: "",
        selected: undefined,
        isLoading: true,
        showToast: false,
        ratingModalVisible: false,
      }
   }

 componentDidMount () {
   this._loadData();
 }

 onStarRatingPress(rating) {
    this.setState({
      starCount: rating
    });
  }

 setRatingModalVisible(visible) {
   this.setState({ratingModalVisible: visible});
 }

 async _loadData () {
   try{
         let response = await AsyncStorage.getItem('loginData');
         let jsonDatas = await JSON.parse(response) || "";

         this.setState({
           "token": jsonDatas.token
         });
       }catch (error) {
        console.log('AsyncStorage error: ' + error.message);
      }
   }

   startService = (serviceId,status) =>{

   DialogProgress.show(options);

   let rand = sha1(new Date().getTime());
   let hash = null;
   let url = null;

   if(status === "start"){
     hash = HASH(API_START_SERVICE,rand);
     url = API_URL + API_START_SERVICE;
   }else if (status === "complete"){
     hash = HASH(API_COMPLETE_SERVICE,rand);
     url = API_URL + API_COMPLETE_SERVICE;
   }

   const formData = new FormData();
   formData.append('hash', hash);
   formData.append('rand', rand);
   formData.append('uuid', deviceId);
   formData.append('token', this.state.token);
   formData.append('serviceId', serviceId)

   let postData = {
     method: 'POST',
     headers: {
         'Accept': 'application/json',
         'Content-Type': 'multipart/form-data'
     },
     body:formData
   }

   console.log(status+" Service postdata:"+ JSON.stringify(postData));

   fetch(url, postData)
      .then(response => {
        return response.json()
      })
      .then((responseJson) => {
        console.log("Start Service response:"+ JSON.stringify(responseJson));

        if(responseJson.settings.success){
          if(!responseJson.settings.message){
            message = "Successfully completed"
          }else{
            message = responseJson.settings.message
          }
          Toast.show({
            text: message,
            textStyle: { color: "green" },
            buttonText: "Dismiss"});

            this.forceUpdate();
        }else{
          Toast.show({
            text: responseJson.settings.message,
            textStyle: { color: "yellow" },
            buttonText: "Dismiss"});
        }

   })
   .catch((error) =>{
     Toast.show({
       text: "Kindly check your network connection and try again",
       textStyle: { color: "red" },
       buttonText: "Dismiss"});
   })

   DialogProgress.hide();
 }


 rateCustomer = (quoteDetails) =>{

 DialogProgress.show(options);

 let rand = sha1(new Date().getTime());
 let hash = HASH(API_RATE_CUSTOMER,rand);
 let url = API_URL + API_RATE_CUSTOMER;

 const formData = new FormData();
 formData.append('hash', hash);
 formData.append('rand', rand);
 formData.append('uuid', deviceId);
 formData.append('token', this.state.token);
 formData.append('customerId', quoteDetails.customerId);
 formData.append('serviceId', quoteDetails.serviceId);
 formData.append('jobId', quoteDetails.jobId);
 formData.append('rating', this.state.starCount);
 formData.append('message', this.state.ratingMessage);


 let postData = {
   method: 'POST',
   headers: {
       'Accept': 'application/json',
       'Content-Type': 'multipart/form-data'
   },
   body:formData
 }

 //console.log("Rating postdata:"+ JSON.stringify(postData));


 fetch(url, postData)
    .then(response => {
      return response.json()
    })
    .then((responseJson) => {
      console.log("Rating response:"+ JSON.stringify(responseJson));

      if(responseJson.settings.success){
        if(!responseJson.settings.message){
          message = "Customer has been rated"
        }else{
          message = responseJson.settings.message
        }
        Toast.show({
          text: message,
          textStyle: { color: "green" },
          buttonText: "Dismiss"});

          this.forceUpdate();
      }else{
        Toast.show({
          text: responseJson.settings.message,
          textStyle: { color: "yellow" },
          buttonText: "Dismiss"});
      }

 })
 .catch((error) =>{
   Toast.show({
     text: "Kindly check your network connection and try again",
     textStyle: { color: "red" },
     buttonText: "Dismiss"});
 })

 DialogProgress.hide();
 this.setRatingModalVisible(!this.state.ratingModalVisible);
}


 onValueChangePicker(value: string) {
    this.setState({
      selected: value
    });
  }

  _toUpper(str) {

   return str .toLowerCase()
           .split(' ')
           .map(function(word) {
               return word[0].toUpperCase() + word.substr(1);
           })
           .join(' ');
 }

  _callNumber(phoneNumber){
    if (Platform.OS !== 'android') {
    phoneNumber = `telprompt:${phoneNumber}`;
    }
    else  {
    phoneNumber = `tel:${phoneNumber}`;
    }

    Linking.canOpenURL(phoneNumber)
    .then(supported => {
    if (!supported) {
        Toast.show({
          text: "Phone number is not available",
          textStyle: { color: "red" },
          buttonText: "Dismiss"});
      } else {
        return Linking.openURL(phoneNumber);
    }
    })
    .catch(err => console.log(err));

  };

  _displayContent(loading,quoteDetails,requestDetails){

    let customerName = requestDetails.customerName;

    quoteSubtotal = <Text style={{marginTop:40,marginBottom:5,fontFamily:"Thasadith-Bold",color:"#FF9900", fontSize:18,}}>SUBTOTALS</Text>
    {messageDisplay}
    <Card>
      <CardItem bordered>
        <Grid>
          <Col><Text>Labour Fee</Text></Col>
          <Col><Text>{FORMAT_NUMBER(quoteDetails.cost)}</Text></Col>
        </Grid>
      </CardItem>
      <CardItem bordered>
        <Grid>
          <Col><Text>Commission Fee</Text></Col>
          <Col><Text>{FORMAT_NUMBER(quoteDetails.commission)}</Text></Col>
        </Grid>
      </CardItem>
      <CardItem bordered>
        <Grid>
          <Col><Text>Taxes</Text></Col>
          <Col><Text>0.00</Text></Col>
        </Grid>
      </CardItem>
      <CardItem bordered>
        <Grid>
          <Col><Text>Other Charges</Text></Col>
          <Col><Text>0.00</Text></Col>
        </Grid>
      </CardItem>
    </Card>

 if(requestDetails.status === "Progress"){

      statusDisplay = <Button iconLeft rounded success onPress={() => this.startService(quoteDetails.serviceId,"complete")}>
        <Icon active name="briefcase" size={25} color="#FFF" style={{marginLeft: 15}} />
        <Text>Complete Work</Text>
      </Button>
    }else if( !requestDetails.status === "Completed") {
      statusDisplay = <Button iconLeft rounded style={{backgroundColor:COLOR_TURGUOISE}} onPress={() => this.startService(quoteDetails.serviceId,"start")}>
        <Icon active name="briefcase" size={25} color="#FFF" style={{marginLeft: 15}} />
        <Text>Start Working</Text>
      </Button>
    }

    if(requestDetails.status === "Completed"){
      if(quoteDetails.custProfile.reviews.length < 1){
        buttonHired =
        <View style={{marginTop:5, marginLeft: 50, marginRight: 50}}>
            <Button iconLeft block rounded style={{backgroundColor:"#FF9900"}} onPress={() => { this.setRatingModalVisible(true);}}>
              <Icon active name="star" size={25} color="#FFF" style={{ marginLeft: 15}} />
              <Text>Rate {customerName}</Text>
            </Button>
        </View>
      }
    }else{
      buttonHired = <Grid style={{marginTop:5}}>
        <Col>
          <Button iconLeft rounded style={{backgroundColor:"#FF9900"}} onPress={() => this._callNumber("+254"+quoteDetails.custProfile.personalInfo.mobileNo)}>
            <Icon active name="phone" size={25} color="#FFF" style={{ marginLeft: 15}} />
            <Text>Call Customer</Text>
          </Button>
        </Col>
        <Col>
          {statusDisplay}
        </Col>
      </Grid>
    }


    if(requestDetails.status === "Completed" || requestDetails.status === "Progress"){

      if(requestDetails.endDate){
        calendarStart =   <View>
            <Text note>Start Date</Text>
            <Text note>{moment(requestDetails.startDate).format('Do MMM YYYY')}</Text>
          </View>

          calendarEnd =   <View>
              <Text note>End Date</Text>
              <Text note>{moment(requestDetails.endDate).format('Do MMM YYYY')}</Text>
            </View>

          quoteSubtotal = <View style={{justifyContent: 'center',alignItems: 'center', marginTop: 50}}>
                              <Image style={{ width: 200,height: 200}} resizeMode="contain" source={completedImage}/>
                          </View>
      }else{

        messageDisplay = <Text note style={{color:COLOR_TURGUOISE}}>Completed: Pending Confirmation from Customer</Text>
        calendarStart =   <View>
            <Text note>Start Date</Text>
            <Text note>{moment(requestDetails.startDate).format('Do MMM YYYY')}</Text>
          </View>

          calendarEnd =   <View>
              <Text note>Service Date</Text>
              <Text note>{moment(requestDetails.jobDate).format('Do MMM YYYY')}</Text>
            </View>
      }

    }else{
      calendarStart =   <View>
          <Text note>Posted Date</Text>
          <Text note>{moment(requestDetails.postedDate).format('Do MMM YYYY')}</Text>
        </View>

        calendarEnd =   <View>
            <Text note>Service Date</Text>
            <Text note>{moment(requestDetails.jobDate).format('Do MMM YYYY')}</Text>
          </View>
    }


    if(loading){
        return <ActivityIndicator size="large" style={styles.activityIndicator}/>
    }else if(quoteDetails.status === "Pending"){
      return (
      <View>
        <Text>{this.state.jsonData}</Text>
        <Text style={styles.textQuote}> What is your Price?</Text>
        <View style={styles.viewPrice}>
          <Text style={styles.textPrice}>Ksh</Text>
          <Item floatingLabel style={{width: 100}}>
            <Input keyboardType = "numeric"
                   style={styles.inputPrice}
                   returnKeyType={ 'next' }
                   placeholder="Price"
                   onChangeText={price => this.setState({price})}/>
          </Item>
        </View>
        <Picker
              mode="dropdown"
              iosIcon={<Icon name="caret-down" />}
              style={styles.pickerRate}
              placeholder="Select your rate"
              placeholderStyle={{ color: "#bfc6ea" }}
              placeholderIconColor="#007aff"
              selectedValue={this.state.selected}
              onValueChange={this.onValueChangePicker.bind(this)}
            >
              <Item label="Fixed Rate" value="fixed-rate" />
              <Item label="Hourly Rate" value="hourly-rate" />
              <Item label="More Info Needed" value="more-info" />
            </Picker>

            <Text style={styles.textQuote}>Write your message to {this._toUpper(customerName)} </Text>
            <Textarea rowSpan={5} bordered returnKeyType={'next'} placeholder="Message" style={{fontFamily:"Thasadith-Regular"}} />

            <Button block rounded
                    style={[c_styles.buttonT25,c_styles.colorOrange]}>
              <Text>Submit</Text>
            </Button>
          </View>
        );
    }else if(quoteDetails.status === "Waiting"){
      return(
        <View>
        <Text style={styles.textTotal}>Total Price</Text>
        <View style={{marginTop: 15}}/>
        <Text style={styles.textCost}>Ksh {FORMAT_NUMBER(quoteDetails.cost)}</Text>
        <Text style={styles.textMessage}>Your quote was successfully sent.</Text>
      </View>
      );
    }else if (quoteDetails.status === "Hired" || quoteDetails.status === "Completed" ){
        var total = parseFloat(quoteDetails.cost)+parseFloat(quoteDetails.commission)
        return(
          <View>
            <View>
              <Card style={{backgroundColor:COLOR_TURGUOISE,width:"107%",height:135, marginTop:-20, marginLeft:-10}}>
                <View>
                  <Text style={{textAlign:"center",color:"#FFF", fontSize:18, marginTop: 10}}>Total Due</Text>
                  <Text style={{textAlign:"center",color:"#FFF", fontSize:24}}>Ksh {FORMAT_NUMBER(total)}</Text>
                  <Text style={{textAlign:"center",color:"#FFF"}} note>Invoice </Text>
                </View>
                <Card style={{position:"absolute",marginTop:105,width:"90%",left:"5%",right:"5%"}}>
                  <CardItem>
                    <Grid>
                      <Col size={1}>
                          <Icon color={COLOR_TURGUOISE} size={30} name="calendar"/>
                      </Col>
                      <Col size={2}>
                        {calendarStart}
                      </Col>
                    </Grid>
                    <Grid>
                      <Col size={1}>
                          <Icon color={COLOR_TURGUOISE} size={30} name="calendar-check-o"/>
                      </Col>
                      <Col size={2}>
                        {calendarEnd}
                      </Col>
                    </Grid>
                  </CardItem>
                </Card>
              </Card>
            </View>
            {quoteSubtotal}
            {buttonHired}
          </View>
        );
    }
  }

  render() {

    let quoteDetails = this.props.screenProps.quoteData;
    let requestDetails = this.props.screenProps.requestData;
    var data = JSON.stringify(requestDetails)

    return (
      <Content padder>

      {/*Rating Modal*/}
      <Modal
        animationType="slide"
        transparent={false}
        visible={this.state.ratingModalVisible}
        onRequestClose={() => {
          Alert.alert('View has been closed.');
        }}>
        <Container style={styles.container}>
          <Header androidStatusBarColor="#9CDEE6" style={c_styles.bgColor}>
          <Left/>
          <Body>
            <Title>Rate {this._toUpper(requestDetails.customerName)}</Title>
          </Body>
            <Right>
              <Button transparent
                onPress={() => {this.setRatingModalVisible(!this.state.ratingModalVisible);}}>
                <Icon name="close" color="#FFF" size={30} />
              </Button>
            </Right>
          </Header>

          <Content padder style={{marginTop: 25}}>
          <Form style={c_styles.formSpacing}>
            <StarRating
            disabled={false}
            emptyStar={'ios-star-outline'}
            fullStar={'ios-star'}
            halfStar={'ios-star-half'}
            iconSet={'Ionicons'}
            maxStars={5}
            rating={this.state.starCount}
            selectedStar={(rating) => this.onStarRatingPress(rating)}
            fullStarColor={COLOR_ORANGE}
            />
            <Item floatingLabel>
              <Label style={c_styles.colorGrey}>Review Message</Label>
              <Input style={{marginTop: 10}}
                  returnKeyType={ 'next' }
                  onChangeText={(ratingMessage) => this.setState({ratingMessage})}/>
            </Item>
          </Form>
          <Button block rounded style={c_styles.buttonT25} onPress={() => this.rateCustomer(quoteDetails)} >
            <Text>Submit</Text>
          </Button>
          </Content>
        </Container>
      </Modal>

      <Text></Text>
        {this._displayContent(this.state.loading,quoteDetails,requestDetails)}
      </Content>
    );
  }
}
