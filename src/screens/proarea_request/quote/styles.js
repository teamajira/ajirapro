import { COLOR_TURGUOISE,COLOR_ORANGE,COLOR_GRAY } from "AjiraPro/src/screens/common_scripts";

export default {
  textQuote: {
    color: COLOR_TURGUOISE,
    fontSize: 16,
    fontFamily: "Thasadith-Bold"
  },
  viewPrice:{
    flex: 1,
    flexDirection:"row",
    alignItems: 'center',
    justifyContent: 'center'
  },
  textPrice:{
    justifyContent: "center",
    alignItems: "center",
    textAlign: "center",
    marginTop:10,
    color: COLOR_ORANGE,
    fontSize: 25
  },
  inputPrice: {
    color:COLOR_ORANGE,
    fontFamily:"Thasadith-Regular",
    fontSize: 22
  },
  pickerRate:{
    justifyContent: "center",
    alignItems: "center",
    marginLeft: 75,
    marginRight:75
  },
  textTotal: {
    color: COLOR_TURGUOISE,
    justifyContent: "center",
    alignItems: "center",
    textAlign: "center",
    marginTop: 50,
    fontSize: 26,
    fontFamily: "Thasadith-Bold"
  },
  textCost: {
    color: COLOR_TURGUOISE,
    justifyContent: "center",
    alignItems: "center",
    textAlign: "center",
    fontSize: 40,
    fontWeight: "bold",
    fontFamily: "Thasadith-Bold"
  },
  textMessage: {
    color: COLOR_ORANGE,
    justifyContent: "center",
    alignItems: "center",
    textAlign: "center",
    fontSize: 16,
    fontFamily: "Thasadith-Bold"
  }
}
