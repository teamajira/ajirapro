import React, { Component } from "react";
import {ImageBackground,Image,View,Keyboard,AsyncStorage,NetInfo} from "react-native";
import {NavigationActions} from 'react-navigation';
import {Container,H1,Header,Title,Content,Button,Item,Label,Input,Body,Left,Right,Icon,Form,Text} from "native-base";
import FormData from 'FormData';
import CryptoJS from 'crypto-js';
import DeviceInfo from 'react-native-device-info';
import DialogProgress from 'react-native-dialog-progress'; {
  /*import RNFirebase from 'react-native-firebase';*/ }

import styles from "./styles";
import common_styles from "../common_styles";
import { HASH,API_URL,APP_KEY,APP_VERSION,API_SET_PASSWORD} from "../common_scripts";

const loginCover = require("AjiraPro/assets/app-cover.png");
const launchscreenLogo = require("AjiraPro/assets/logopro.png");

let sha1 = require('sha1');

const options = {
  title: "Signing In",
  message: "Please wait...",
  isCancelable: true
}

const deviceId = DeviceInfo.getUniqueID();
const osVersion = DeviceInfo.getVersion();

class SetPassword extends Component {

  constructor(props) {
    super(props);

    this.state = {
      code: "",
      password: "",
      confirmPassword:"",
      SMScode:"",
      isLoading: true,
      loginData: "",
      address: "Not Available",
      firebaseToken: "",
      backgroundColor: "transparent",
      hidden: true,
      jsonData: "",
      iconName: "md-eye-off"
    }

  }

  onFocus = () => {
    this.setState({
      backgroundColor: "#fff7eb"
    })
  }

  onBlur = () => {
    this.setState({
      backgroundColor: "transparent"
    })
  }

  hidePassword = () => {
    this.setState({
      hidden: !this.state.hidden
    })
    if (this.state.hidden) {
      this.setState({
        iconName: "md-eye"
      })
    } else {
      this.setState({
        iconName: "md-eye-off"
      })
    }
  }

  componentDidMount() {

     this._loadData();

  }

  async _loadData () {
    try {
          let response = await AsyncStorage.getItem('otpCodeData');
          let jsonDatas = await JSON.parse(response) || "";

          this.setState({
            "code": jsonDatas.code
          });

          console.log('set otpCodeData: ' + this.state.code);

        }catch (error) {
         console.log('AsyncStorage error: ' + error.message);
       }
    }


  UserSetPasswordAction = () => {

    DialogProgress.show(options);

    rand = sha1(new Date().getTime());
    this.url = API_URL + API_SET_PASSWORD;

    // const {
    //   code
    // } = this.state;
    // const {
    //   SMScode
    // } = this.state;
    // const {
    //   password
    // } = this.state;
    const navigateAction = NavigationActions.navigate({
      routeName: 'Login'
    })

    const formData = new FormData();
    formData.append('hash', HASH(API_SET_PASSWORD, rand));
    formData.append('rand', rand);
    formData.append('uuid', deviceId);
    formData.append('osVersion', osVersion);
    formData.append('code', this.state.code);
    formData.append('SMScode', this.state.SMScode);
    formData.append('password', this.state.password);
    formData.append('confirmPassword', this.state.confirmPassword);

    let postData = {
      method: 'POST',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'multipart/form-data'
      },
      body: formData
    }

    console.log("login postData:" + JSON.stringify(postData));

    NetInfo.isConnected.fetch().then((isConnected) => {
      if (isConnected) {
        fetch(this.url, postData)
          .then(response => {
            return response.json()
          })
          .then((responseJson) => {

            console.log("login response:" + JSON.stringify(responseJson));
            let salt = responseJson.data.otpSalt;

            Keyboard.dismiss();
            if (responseJson.settings.success) {
              this.props.navigation.dispatch(navigateAction);
              alert('Your new password was set successfully');
            } else {
              alert(responseJson.settings.message);
            }
          })
          .catch((error) => {
            DialogProgress.hide()
            console.log(JSON.stringify(error));
            if (error == 'TypeError: Network request failed') {
              alert('Unable to login, contact administrator', );
            }
          })
      } else {
        DialogProgress.hide()
        alert("Kindly check if the device is connected to stable cellular data connection or WiFi.");
      }
    });


    DialogProgress.hide()
  }
  render() {
    return (
      <Container style={common_styles.container}>
      <ImageBackground source={loginCover} style={styles.cover} >
          <View style={styles.logoContainer}>
            <Image source={launchscreenLogo} style={common_styles.logo} />
          </View>
          <Header androidStatusBarColor="#9CDEE6" style={{ display: "none" }} />
      </ImageBackground>
        <Content>
          <View
            style={{
              alignItems: "center",
              marginTop: 15,
              backgroundColor: "transparent"
            }}>
            <View style={{ marginTop: 10 }} />
            <H1 style={styles.text}>Set Password</H1>
          </View>
          <Form style={common_styles.formSpacing}>
            <Item floatingLabel>
              <Label style={common_styles.colorGrey}>SMS Code</Label>
              <Input style={{marginTop: 10}}
                  keyboardType = "numeric"
                  returnKeyType={ 'next' }
                  onBlur={ () => this.onBlur() }
                  onFocus={ () => this.onFocus() }
                  onChangeText={(SMScode) => this.setState({SMScode})}/>
            </Item>
            <Item floatingLabel>
              <Label>Password</Label>
              <Input
                style={{marginTop: 10}}
                returnKeyType={ 'next' }
                value={ this.state.password }
                onBlur={ () => this.onBlur() }
                onFocus={ () => this.onFocus() }
                secureTextEntry={this.state.hidden}
                onChangeText={password => this.setState({password})}/>
                <Icon name={this.state.iconName} onPress={() => this.hidePassword()} style={{color:"#333"}} />
            </Item>
            <Item floatingLabel>
              <Label>Confirm Password</Label>
              <Input
                style={{marginTop: 10}}
                returnKeyType={ 'next' }
                value={ this.state.confirmPassword }
                onBlur={ () => this.onBlur() }
                onFocus={ () => this.onFocus() }
                secureTextEntry={this.state.hidden}
                onChangeText={confirmPassword => this.setState({confirmPassword})}/>
                <Icon name={this.state.iconName} onPress={() => this.hidePassword()} style={{color:"#333"}} />
            </Item>
          </Form>
          <Button block rounded
                  style={common_styles.buttonT25}
                  onPress={this.UserSetPasswordAction}>
            <Text>Set Password</Text>
          </Button>

          <Text style={[common_styles.alignCenter,common_styles.colorGrey,common_styles.underline]}
                onPress={() => this.props.navigation.navigate('Login')}>
                 Back to Login ?
          </Text>
        </Content>
      </Container>
    );
  }
}

export default SetPassword;
