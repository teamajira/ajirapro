const React = require("react-native");
const { StyleSheet, Dimensions, Platform } = React;
const deviceHeight = Dimensions.get("window").height;

export default {
  cover:{
    width: '100%',
    height: 200
  },
  logoContainer: {
    flex: 1,
    marginTop: deviceHeight / 4,
    marginTop: 10,
    resizeMode: 'contain'
  },
  underline: {
    textDecorationLine: 'underline'
  }
 };
