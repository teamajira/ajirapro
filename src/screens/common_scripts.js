/* Imported Packages*/
import CryptoJS from 'crypto-js';
import {AsyncStorage } from "react-native";

/*Variables*/
var sha1 = require('sha1');

/*Colors*/
export const COLOR_PRIMARY = '#36a587';
export const COLOR_PRIMARY_DARKER = '#206351';
export const COLOR_ACCENT = '#FF4081';
export const FONT_NORMAL = 'OpenSans-Regular';
export const FONT_BOLD = 'OpenSans-Bold';
export const BORDER_RADIUS = 5;

export const COLOR_ORANGE = '#FF9900';
export const COLOR_TURGUOISE ='#08aec2'; //'#06B7CC'
export const COLOR_GRAY = '#c0c0c0';
export const COLOR_WHITE = '#FFFFFF';

//app version
export const APP_VERSION = "ADC1.0";

//connection props
//export const APP_KEY = "BeyaAALEcZ9szdXqd64S2cxhL4fmVUfZ"; //live api key
export const APP_KEY = "xreA3uebtGNEwF3gNDMZr3T8C8sT3jzp"; //demo api key
//export const API_URL = "https://apiv5.ajiraconnect.com/";
export const API_URL = "https://apiv2demo.ajiraconnect.com/";

//endpoints
export const API_REGISTER_INDIVIDUAL = "register/individual";
export const API_REGISTER_BUSINESS = "register/business";
export const API_REQUEST_OTP = "otp/request";
export const API_VERIFY_OTP = "otp/verify";

export const API_CATEGORY = "category/list";
export const API_SUBCATEGORY = "subcategory/list";

export const API_LOGIN = "login/mobilepro";
export const API_LEGAL = "staticpage/list";

export const API_REQUESTS = "proindividual/servicerequested";
export const API_HIRED = "proindividual/servicehired";
export const API_COMPLETED = "proindividual/servicecompleted";
export const API_START_SERVICE = "proindividual/startservice";
export const API_COMPLETE_SERVICE = "proindividual/completeservice";
export const API_RATE_CUSTOMER = "proindividual/ratecustomer";

export const API_PAYMENT = "wallet/withdrawals";
export const API_NOTIFICATIONS = "notification/list";
export const API_NOTIFICATIONS_READ = "notification/read";

export const API_PROFILE = "proindividual/profile";
export const API_CHANGE_PASS = "changepassword/change";

export const API_UPDATE_PERSONAL = "proindividual/personal";
export const API_UPDATE_ABOUT = "proindividual/about";
export const API_UPLOAD_PICTURE = "proindividual/pictureupload";
export const API_DELETE_PICTURE = "proindividual/picturedelete";
export const API_FORGOT_PASSWORD = "resetpassword/reset";
export const API_SET_PASSWORD = "resetpassword/verify";

//functions
export const RAND = sha1(new Date().getTime());


export const CryptoJS512 = function(data) {
  return CryptoJS.SHA512(data).toString(CryptoJS.enc.Hex);
}

export const HASH = function(call, rand) {

  this.currentDate = new Date();
  this.month = this.currentDate.getMonth() + 1;
  this.currentMonth = this.month < 10 ? '0' + this.month : '' + this.month
  this.currentYear = this.currentDate.getFullYear();
  this.dateValue = this.currentMonth.toString() + this.currentYear.toString();

  return APP_VERSION + '' + CryptoJS512(CryptoJS512(rand) + CryptoJS512(APP_KEY) + CryptoJS512(rand + call + this.dateValue));
}

export const FORMAT_NUMBER = function(x) {
  if (x) {
    var parts = x.toString().split(".");
    parts[0] = parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ",");
    return parts.join(".");
  } else {
    return 0;
  }
}

export const NOTIFICATION_COUNT = function(){
  try{
    let response = AsyncStorage.getItem('notificationCount');
    if(parseFloat(response) > 0){
      return <Badge style={{ position: 'absolute',marginLeft: 30 }}><Text>2</Text></Badge>
    }

  }catch (error) {
    console.log('AsyncStorage error: ' + error.message);
  }
}
