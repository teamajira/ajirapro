const React = require("react-native");
const { StyleSheet, Dimensions, Platform } = React;
const deviceHeight = Dimensions.get("window").height;

import {
  COLOR_TURGUOISE,
  COLOR_ORANGE,
  COLOR_GRAY,
  COLOR_WHITE
} from "AjiraPro/src/screens/common_scripts";

export default {
  backgroundContainer: {
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: COLOR_WHITE
  },
  logo: {
    width: 200,
    height: 200
  },
  logoContainer: {
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: 50
  },
  bgSlogan:{
    alignItems: "center",
    marginBottom: 15,
    marginTop:25,
    backgroundColor: "transparent"
  },
  text: {
    color: COLOR_GRAY,
    bottom: 6,
    marginTop: 5
  },
  textWhite: {
    color: COLOR_WHITE,
    bottom: 6,
    marginTop: 5
  },
  textOrange: {
    color: COLOR_ORANGE,
    bottom: 6,
    marginTop: 5
  },
  textTheme: {
    color: COLOR_TURGUOISE,
    bottom: 6,
    marginTop: 5
  },
  textSlogan: {
    color: COLOR_TURGUOISE,
    bottom: 50,
    marginTop: 75
  },
  mb15: {
    marginBottom: 20
  },
  mt15: {
    marginTop: 15
  },
  mb20: {
    marginBottom: 20
  },
  bgColor: {
    backgroundColor: COLOR_WHITE
  },
  bgTheme: {
    backgroundColor: COLOR_TURGUOISE
  },
  bgOrange: {
    backgroundColor: COLOR_ORANGE
  },
  underline: {
    textDecorationLine: 'underline'
  }
};

/* D8D8D8*/
