import React, { Component } from "react";
import {NavigationActions} from 'react-navigation';
import { ActivityIndicator,ImageBackground,StatusBar,Modal, View, Alert,ScrollView, Dimensions } from "react-native";
import { Container,Button,H1,Header,Title,Content,Icon,Text,Left,Right,Body} from "native-base";
import HTML from 'react-native-render-html';
import FormData from 'FormData';
import {AsyncStorage,NetInfo } from "react-native";
import DeviceInfo from 'react-native-device-info';

import styles from "./styles";
import c_styles from "../common_styles";

const launchscreenLogo = require("AjiraPro/assets/logo.png");

import { HASH,API_URL,APP_KEY,APP_VERSION, API_LEGAL }  from "../common_scripts";

let sha1 = require('sha1');
const deviceId = DeviceInfo.getUniqueID();
const osVersion = DeviceInfo.getVersion();

class Home extends Component {

  constructor() {
    super();
    this.state = {
      termsModalVisible: false,
      privacyModalVisible: false,
      jsonData: "",
      termsContent: "",
      policyContent:""
     };
  }

  componentDidMount () {
    this._isMounted = true;
    this.legalData();
  }

  setTermModalVisible(visible) {
    this.setState({termsModalVisible: visible});
  }

  setPrivacyModalVisible(visible) {
    this.setState({privacyModalVisible: visible});
  }

  legalData = () =>{

     rand = sha1(new Date().getTime());
     this.url = API_URL + API_LEGAL;

     const formData = new FormData();
     formData.append('hash', HASH(API_LEGAL,rand));
     formData.append('rand', rand);
     formData.append('uuid', deviceId);
     formData.append('osVersion', osVersion);
     let postData = {
       method: 'POST',
       headers: {
           'Accept': 'application/json',
           'Content-Type': 'multipart/form-data'
       },
       body:formData
     }

     NetInfo.isConnected.fetch().then((isConnected) => {
       if(isConnected){
        fetch(this.url, postData)
        .then(response => {
          return response.json()
        })
        .then((responseJson) => {

          if (this._isMounted) {

            this.setState({
              policyContent: responseJson.data.staticpages[2].content,
              termsContent: responseJson.data.staticpages[3].content
            });
          }
        })
       .catch((error) =>{
         })
      }
   });
  }

  componentWillUnmount() {
    this._isMounted = false;
  }

  render() {

      return (
        <Container style={styles.bgColor}>
          <Header transparent androidStatusBarColor="#ffffff" style={{display:'none'}}>
            <Right />
          </Header>

          {/*Terms and Conditions*/}
          <Modal
            animationType="slide"
            transparent={false}
            visible={this.state.termsModalVisible}
            onRequestClose={() => {
              Alert.alert('View has been closed.');
            }}>
            <Container style={styles.container}>
              <Header androidStatusBarColor="#9CDEE6" style={c_styles.bgColor}>
              <Left/>
              <Body>
                <Title> Terms Of Use</Title>
              </Body>
                <Right>
                  <Button transparent
                    onPress={() => {this.setTermModalVisible(!this.state.termsModalVisible);}}>
                    <Icon name="md-close" />
                  </Button>
                </Right>
              </Header>
              <Content padder>
                <ScrollView style={{ flex: 1 }}>
                 <HTML html={this.state.termsContent} />
                </ScrollView>
              </Content>
            </Container>
          </Modal>

          {/*Privacy Policy*/}
          <Modal
            animationType="slide"
            transparent={false}
            visible={this.state.privacyModalVisible}
            onRequestClose={() => {
              Alert.alert('View has been closed.');
            }}>
            <Container style={styles.container}>
              <Header androidStatusBarColor="#9CDEE6" style={c_styles.bgColor}>
              <Left/>
              <Body>
                <Title> Privacy Policy</Title>
              </Body>
                <Right>
                  <Button transparent
                    onPress={() => {this.setPrivacyModalVisible(!this.state.privacyModalVisible);}}>
                    <Icon name="md-close" />
                  </Button>
                </Right>
              </Header>
              <Content padder>
                <ScrollView style={{ flex: 1 }}>
                 <HTML html={this.state.policyContent} />
                </ScrollView>
              </Content>
            </Container>
          </Modal>

          <Content padder>
            <View style={styles.logoContainer}>
              <ImageBackground source={launchscreenLogo} style={styles.logo} resizeMode="contain" />
            </View>
            <View
              style={styles.bgSlogan}>
              <H1 style={styles.textSlogan}>Grow Your Business</H1>
            </View>

            <View>
                <Button block rounded light
                style={[styles.bgTheme,styles.mb15]}
                onPress={() => this.props.navigation.navigate('Login')}>
                  <Text style={styles.textWhite}>Log In</Text>
                </Button>

                <Button block rounded light
                style={[styles.bgOrange,styles.mb15]}
                onPress={() => this.props.navigation.navigate('Register')}>
                  <Text style={styles.textWhite}>Register</Text>
                </Button>
              </View>

              <View style={c_styles.formSpacing}>
                <Text style={styles.text}> By tapping register, log In , I agree to Ajira Connect's
                  <Text>  </Text>
                  <Text style={[styles.textOrange,styles.textBold,styles.underline]}
                        onPress={() => { this.setTermModalVisible(true);}}>Terms of Service
                  </Text>
                  <Text style={styles.text}> and </Text>
                  <Text style={[styles.textOrange,styles.textBold,styles.underline]}
                        onPress={() => { this.setPrivacyModalVisible(true);}}>Privacy Policy
                  </Text>
                </Text>
              </View>

              <Text>{this.state.jsonData}</Text>

          </Content>
        </Container>
      );

  }
}

export default Home;
