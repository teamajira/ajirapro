import React, { Component } from "react";
import { version } from 'AjiraPro/package.json';
import {NavigationActions} from 'react-navigation';
import { Image,View,AsyncStorage } from "react-native";
import {Content,Text,List,ListItem,Icon,Container,Left,Right,Badge,Body,Switch,Thumbnail,H2} from "native-base";

import styles from "./style";
const datas = [
  {
    name: "Profile",
    route: "Profile",
    icon: "person",
    bg: "#477EEA"
  },
  {
    name: "Payments",
    route: "Payments",
    icon: "md-cash",
    bg: "#C5F442"
  },
  {
    name: "Change Password",
    route: "ChangePassword",
    icon: "key",
    bg: "#477EEA"
  },
  {
    name: "Notifications",
    route: "Notifications",
    icon: "notifications",
    bg: "#477EEA"
  },
  {
    name: "Legal",
    route: "Legal",
    icon: "paper",
    bg: "#C5F442"
  }
];

const header = {
  name: "Pro Area",
  route: "ProArea",
  icon: "home",
  bg: "#C5F442"
};

const headerDocs = {
  name: "Professional Details",
  route: "Documents",
  icon: "home",
  bg: "#C5F442"
};

let sideBarList = [];
let allSideBarList = [];

class SideBar extends Component {

  constructor(props) {
    super(props);
    this.state = {
      shadowOffsetWidth: 1,
      shadowRadius: 4,
      name: "",
      profileBanner: require("AjiraPro/assets/profile_banner.png")
    };
  }

  componentDidMount () {
    this._loadData();
  }

  _loadData () {
    let photoUrl =  null;
    let name = null;
    let errorCode = null;

    AsyncStorage.getItem('errorCode').then((errorCode) => {
      if(errorCode === "Inactive"){
        if(sideBarList.every((item) => item.route !== "Documents")){
          sideBarList.push(headerDocs);
        }
      }else{
        if(sideBarList.every((item) => item.route !== "ProArea")){
          sideBarList.push(header);
        }
      }

      allSideBarList = sideBarList.concat(datas)
      console.log("Data "+JSON.stringify(allSideBarList));
    });

    AsyncStorage.getItem('personalPhoto').then((photoUrlStatus) => {
      if(photoUrlStatus){
        this.setState({
          profileBanner: {uri: photoUrlStatus},
        });
      }

      console.log("Banner "+this.state.profileBanner);
    });

    AsyncStorage.getItem('firstName').then((nameStatus) => {
      if(nameStatus){
        this.setState({
          name: this._toUpper(nameStatus)
        });
      }
      
      console.log("Name "+this.state.name);
    });

  }

  async _userLogout(){
    try{
      await AsyncStorage.clear();;
      const navigateAction = NavigationActions.navigate({
        routeName: 'Home'
      })
      this.props.navigation.dispatch(navigateAction)
    }catch (error) {
      console.log('AsyncStorage error: ' + error.message);
    }
  }

  _toUpper(str) {

    if(str && str !== null){
      return str .toLowerCase()
      .split(' ')
      .map(function(word) {
        return word[0].toUpperCase() + word.substr(1);
      })
      .join(' ');
    }
  }

  render() {
    return (
      <Container>
        <Content
          bounces={false}
          style={{ flex: 1, backgroundColor: "#fff", top:-1}}>

          <View style={styles.imageHolder}>
            <Thumbnail large source={this.state.profileBanner} style={styles.profileImage} />
            <Right style={{marginRight: 25}}>
              <Left>
                <H2 style={styles.textName}>{this.state.name}</H2>
              </Left>
              <View/>
              <Text style={styles.textVersion}>Version: {version} </Text>
            </Right>
          </View>
          <List
            dataArray={allSideBarList}
            renderRow={data =>
              <ListItem
                button
                noBorder
                onPress={() => this.props.navigation.navigate(data.route)}>
                <Left>
                  <Icon
                    active
                    name={data.icon}
                    style={{ color: "#777", fontSize: 26, width: 30 }}
                  />
                  <Text style={styles.text}>
                    {data.name}
                  </Text>
                </Left>
                {data.types &&
                  <Right style={{ flex: 1 }}>
                    <Badge
                      style={{
                        borderRadius: 3,
                        height: 25,
                        width: 72,
                        backgroundColor: data.bg
                      }}>
                      <Text
                        style={styles.badgeText}
                      >{`${data.types} Types`}</Text>
                    </Badge>
                  </Right>}
              </ListItem>}
              />

              <ListItem button noBorder>
                <Left >
                  <View style={{marginLeft: 30}} />
                  <Text style={styles.text} >Go Online</Text>
                </Left>
                <Body/>
                <Right>
                  <Switch value={false} />
                </Right>
              </ListItem>

              <ListItem button noBorder onPress={() => { this._userLogout();}}>
                <Left>
                  <Icon active name="log-out" style={{ color: "#777", fontSize: 26, width: 30 }} />
                  <Text style={styles.text}>Log Out</Text>
                </Left>
                <Body/>
                <Right/>
              </ListItem>
        </Content>
      </Container>
    );
  }
}

export default SideBar;
