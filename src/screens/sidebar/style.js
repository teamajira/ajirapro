const React = require("react-native");
const { Platform, Dimensions } = React;

const deviceHeight = Dimensions.get("window").height;
const deviceWidth = Dimensions.get("window").width;

export default {
  drawerCover: {
    alignSelf: "stretch",
    height: deviceHeight / 3.5,
    width: null,
    position: "relative",
    marginBottom: 5
  },
  drawerImage: {
    position: "absolute",
    left: Platform.OS === "android" ? deviceWidth / 10 : deviceWidth / 9,
    top: Platform.OS === "android" ? deviceHeight / 13 : deviceHeight / 12,
    width: 210,
    height: 75,
    resizeMode: "cover"
  },
  imageHolder:{
    height: 160 ,
    width: 450,
    padding: 10,
    paddingTop: Platform.OS === "android" ? 25 : 50,
    backgroundColor: "#08aec2"
  },
  profileImage: {
    backgroundColor: "#fff",
    height: 125,
    width: 125,
    borderRadius: 125/2
  },
  text: {
    fontWeight: Platform.OS === "ios" ? "500" : "400",
    fontSize: 16,
    marginLeft: 20
  },
  badgeText: {
    fontSize: Platform.OS === "ios" ? 13 : 11,
    fontWeight: "400",
    textAlign: "center",
    marginTop: Platform.OS === "android" ? -3 : undefined
  },
  textVersion: {
    color: "#FFF",
    bottom: 15,
  },
  textName: {
    marginTop:-125,
    color: "#FFF" ,
    marginBottom: 5
  }
};
