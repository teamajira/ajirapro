import React, { Component } from "react";
import { NavigationActions } from 'react-navigation';
import { ActivityIndicator,AsyncStorage,StatusBar,Image,View } from "react-native";
import { Container,Header,Content,Left,Right,Body,Text} from "native-base";

import styles from "./styles";
import common_styles from "../common_styles";

const launchscreenLogo = require("../../../assets/ic_launcher.png");
const navigateProArea = NavigationActions.navigate({routeName: 'ProArea'})
const navigateHome = NavigationActions.navigate({routeName: 'Home'})
const navigateDocuments = NavigationActions.navigate({routeName: 'Documents'})

let errorCode =  null;
class Home extends Component {

  componentDidMount () {
    AsyncStorage.getItem('errorCode')
      .then((errorCodeStatus) => {

        if(errorCodeStatus){
            errorCode = errorCodeStatus
          }
    });

    this.timeoutHandle = setTimeout(()=>{
        if (errorCode === "L002" || errorCode === null){
          return this.props.navigation.dispatch(navigateHome);
        }else if(errorCode === "Inactive"){
          return this.props.navigation.dispatch(navigateDocuments);
        }else if (errorCode === "Active"){
          return this.props.navigation.dispatch(navigateProArea);
        }
    }, 1000);
  }

  componentWillUnmount () {
    clearTimeout(this.timeoutHandle);
  }

  render() {
      return (
        <Container style={common_styles.bgColor}>
          <Header transparent androidStatusBarColor="#9CDEE6" style={{display:'none'}}/>
          <Content>
            <View style={styles.logoContainer}>
              <Image source={launchscreenLogo} style={styles.logo} />
            </View>
            <View
              style={{
                alignItems: "center",
                marginBottom: 30,
                backgroundColor: "transparent"
              }}>
              <View />
                <ActivityIndicator color="#fff" size="large" style={styles.activityIndicator}/>
            </View>
            <Text>{this.token}</Text>
           </Content>
        </Container>
      );
  }
}

export default Home;
