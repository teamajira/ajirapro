import { COLOR_TURGUOISE } from '../common_scripts';

export default {
  logoContainer: {
    alignItems:  'center',
    resizeMode:'contain'
  },
  logo: {
    flex: 1,
    height: 150,
    width: 150,
    marginTop: 100,
    resizeMode:'cover',
  },
  activityIndicator: {
    marginTop:25
  }
}
