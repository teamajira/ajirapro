const React = require("react-native");
const {
  StyleSheet,
  Dimensions,
  Platform
} = React;
const deviceHeight = Dimensions.get("window").height;

import {
  COLOR_TURGUOISE,
  COLOR_ORANGE,
  COLOR_GRAY
} from "./common_scripts";

export default {
    Textareatyle: {
    width: 200,
    height:100,
    borderColor: 'gray',
    borderWidth: 1
  },
  container: {
    backgroundColor: "#fff"
  },
  logoContainer: {
    flex: 1,
    marginTop: deviceHeight / 8,
    marginTop: 40,
    aspectRatio: 1.5,
    resizeMode: 'contain'
  },
  logo: {
    position: "absolute",
    left: Platform.OS === "android" ? 30 : 50,
    top: Platform.OS === "android" ? 35 : 60,
    width: 280,
    height: 100
  },
  text: {
    color: "#FFF",
    bottom: 6,
    marginTop: 5
  },
  textOrange: {
    color: COLOR_ORANGE
  },
  textTheme: {
    color: COLOR_TURGUOISE,
  },
  textBold: {
    color: "#FFF",
    fontFamily: "Thasadith-Bold",
    bottom: 6,
    marginTop: 5
  },
  textStatus: {
    color: COLOR_TURGUOISE,
    fontSize: 17,
    width: 85,
    textAlign: "center",
    fontFamily: "Thasadith-Bold"
  },
  textName: {
    color: COLOR_TURGUOISE,
    fontSize: 16,
  },
  mmT50: {
    margin: 15,
    marginTop: 50,
    backgroundColor: COLOR_TURGUOISE
  },
  buttonT25: {
    margin: 15,
    marginTop: 25,
    backgroundColor: COLOR_TURGUOISE
  },
  mmT15: {
    margin: 15,
    marginTop: 15,
    backgroundColor: COLOR_TURGUOISE
  },
  marginSide15:{
    marginLeft:15,
    marginRight:15
  },
  roundLeft:{
    borderBottomLeftRadius: 15,
    borderTopLeftRadius: 15
  },
  roundRight:{
    borderBottomRightRadius: 15,
    borderTopRightRadius: 15
  },
  underline: {
    textDecorationLine: 'underline'
  },
  italic: {
    fontStyle: 'italic'
  },
  bold: {
    fontFamily: "Thasadith-Bold"
  },
  colorGrey: {
    color: '#868686'
  },
  alignCenter: {
    textAlign: 'center',
    marginTop: 10,
    marginBottom: 5
  },
  alignRight: {
    textAlign: 'right',
    marginTop: 10,
    marginRight: 5
  },
  formSpacing: {
    margin: 20,
    marginTop: -10
  },
  listBody: {
    flex: 1,
    flexDirection: "row",
    justifyContent: "center",
    alignItems: "center"
  },
  iconList: {
    color: COLOR_ORANGE,
    fontSize: 40,
    alignSelf: 'center'
  },
  seperator: {
    width: 1,
    margin: 5,
    height: 85,
    backgroundColor: COLOR_GRAY,
  },
  activityIndicator: {
    marginTop: 50,
    color: COLOR_TURGUOISE
  },
  colorOrange: {
    backgroundColor: COLOR_ORANGE
  },
  bgColor: {
    backgroundColor: COLOR_TURGUOISE
  },
};
