import React, { Component } from "react";
import {Container,Header,Title,Content,Button,Badge,Icon,Left,Right,Body,Text,Input,Item, Picker, Form, Label} from "native-base";
import { View, NetInfo,AsyncStorage, Image } from "react-native";
import common_styles from "../../common_styles";
import DatePicker from 'react-native-datepicker';//
import { GooglePlacesAutocomplete } from 'react-native-google-places-autocomplete';

import FormData from 'FormData';
import CryptoJS from 'crypto-js';
import DeviceInfo from 'react-native-device-info';
import DialogProgress from 'react-native-dialog-progress';

import { HASH,API_URL,APP_KEY,APP_VERSION, API_UPDATE_PERSONAL,COLOR_ORANGE, CryptoJS512 }  from "../../common_scripts";

let sha1 = require('sha1');
const options = {
  title:"Loading Data",
  message:"Please wait...",
  isCancelable:true
}

const deviceId = DeviceInfo.getUniqueID();
const osVersion = DeviceInfo.getVersion();

let requestData = null;

class UpdateProfile extends React.Component {

  constructor(props){
    super(props);

    this.rand = sha1(new Date().getTime());

    this.currentDate = new Date();
    this.month = this.currentDate.getMonth() + 1;
    this.currentMonth = this.month < 10 ? '0' + this.month : '' + this.month
    this.currentYear = this.currentDate.getFullYear();
    this.dateValue = this.currentMonth.toString() + this.currentYear.toString();

    this.generateHash = function(rand) {
      return APP_VERSION + '' + CryptoJS512(CryptoJS512(rand) + CryptoJS512(APP_KEY) + CryptoJS512(rand + API_UPDATE_PERSONAL + this.dateValue));
    }

    this.hash = this.generateHash(this.rand);
    this.url = API_URL + API_UPDATE_PERSONAL;

    const { navigation } = this.props;

    let email_address = navigation.getParam('email_address', '');
    let mobileNo = navigation.getParam('mobileNo', '');
    //let address = navigation.getParam('location', 'Set Your Location');
    let occupation = navigation.getParam('occupation', '');
    let dob = navigation.getParam('dob', '');
    let gender = navigation.getParam('gender', '');
    let firstName = navigation.getParam('firstName', '');
    let website = navigation.getParam('website', '');
    let lastName = navigation.getParam('lastName', '');
    let address = navigation.getParam('address', '');//"Set Your Location";

    console.log('Received dob-----'+dob);


    this.state = {
      jsonData: "",
      token: "",
      email: JSON.parse(JSON.stringify(navigation.getParam('email_address'))),
      mobileNo:JSON.parse(JSON.stringify(navigation.getParam('mobileNo'))),
      firstName: firstName,
      lastName:lastName,
      occupation:occupation,
      address:address,
      date:JSON.parse(JSON.stringify(navigation.getParam('dob'))),
      website:JSON.parse(JSON.stringify(navigation.getParam('website'))),
      gender:JSON.parse(JSON.stringify(navigation.getParam('gender'))),
      city:JSON.parse(JSON.stringify(navigation.getParam('city'))),
      state:JSON.parse(JSON.stringify(navigation.getParam('state'))),
      zip:JSON.parse(JSON.stringify(navigation.getParam('zip'))),
      countryCode:JSON.parse(JSON.stringify(navigation.getParam('countryCode'))),
      lat:JSON.parse(JSON.stringify(navigation.getParam('lat'))),
      lon:JSON.parse(JSON.stringify(navigation.getParam('lon'))),
    }

  }

  onValueChange(value: string) {
    this.setState({
      gender: value
    });
  }


  componentDidMount() {

    this._loadData();

  }

  async _loadData () {
    try{
      let response = await AsyncStorage.getItem('loginData');
      let jsonDatas = await JSON.parse(response) || "";

      this.setState({
        "token": jsonDatas.token
      });

      console.log('set Token: ' + this.state.token);

      //this.getProfile();
      //DialogProgress.hide()
    }catch (error) {
      console.log('AsyncStorage error: ' + error.message);
    }
  }

  UpdateProfileAction = () =>{

    if(this.state.firstName ==null) {
      alert("Please enter your First Name");
    }
    else if(this.state.lastName ==null) {
      alert("Please enter your Last Name");
    }
    else if(this.state.email ==null) {
      alert("Please enter your Email Address");
    }
    else if(this.state.mobileNo ==null) {
      alert("Please enter your Mobile Number");
    }
    else if(this.state.address ==null) {
      alert("Please enter your Location");
    }
    // else if(this.state.occupation ==null) {
    //   alert("Please enter your Occupation");
    // }
    else if(this.state.gender ==null) {
      alert("Please select your Gender");
    }
    else {

      DialogProgress.show(options);

      let rand = sha1(new Date().getTime());
      let hash = this.generateHash(rand);
      let url = API_URL + API_UPDATE_PERSONAL;

      const formData = new FormData();
      formData.append('hash', hash);
      formData.append('rand', rand);
      formData.append('uuid', deviceId);
      formData.append('token', this.state.token);
      formData.append('email', this.state.email);
      formData.append('dob', this.state.date);
      formData.append('mobileNo', this.state.mobileNo);
      formData.append('occupation', this.state.occupation);
      formData.append('gender', this.state.gender);
      formData.append('firstName', this.state.firstName);
      formData.append('lastName', this.state.lastName);
      formData.append('website', this.state.website);
      formData.append('address', this.state.address);
      formData.append('city', this.state.city);
      formData.append('state', this.state.state);
      formData.append('zip', this.state.zip);
      formData.append('countryCode', this.state.countryCode);
      formData.append('lat', this.state.lat);
      formData.append('lon', this.state.lon);
      formData.append('address', this.state.address);

      let postData = {
        method: 'POST',
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'multipart/form-data'
        },
        body:formData
      }

      console.log("UpdateProfile postdata:"+ JSON.stringify(postData));

      NetInfo.isConnected.fetch().then((isConnected) => {
        if(isConnected){
          fetch(url, postData)
          .then(response => {
            console.log('response---'+JSON.stringify(response));
            return response.json()
          })
          .then((responseJson) => {
            console.log("history response:"+ JSON.stringify(responseJson));

            if(responseJson.settings.success){
              alert('Profile details were updated successfully');

            }
            else {
              alert(JSON.stringify(responseJson.settings.errors));
            }

          })
          .catch((error) =>{
            console.log('Erroor---'+JSON.stringify(error));
            if(error == 'TypeError: Network request failed'){
              alert('Kindly check if the device is connected to stable cellular data connection or WiFi.');
            }
          })

          DialogProgress.hide();
        }else{
          DialogProgress.hide()
          alert("Kindly check if the device is connected to stable cellular data connection or WiFi.");
        }
      }).catch((error)=>{
        console.log("Api call error");
        alert(error.message);
      }).done();

    }
  }

  render() {

    const GooglePlacesInput = () => {
      return (
        <GooglePlacesAutocomplete
        onChangeText={(address) => this.setState({ address })}
        placeholder='Search Location'
        minLength={2} // minimum length of text to search
        autoFocus={false}
        returnKeyType={'search'} // Can be left out for default return key https://facebook.github.io/react-native/docs/textinput.html#returnkeytype
        listViewDisplayed='auto'    // true/false/undefined
        fetchDetails={true}
        renderDescription={row => row.description} // custom description render
        onPress={(data, details = null) => { // 'details' is provided when fetchDetails = true
        console.log('Map-Data---'+data, details);
      }}

      getDefaultValue={() => this.state.address}

      query={{
        // available options: https://developers.google.com/places/web-service/autocomplete
        key: 'AIzaSyA2yzGtTFA0X_94k99VA76Orrrn0u3SUVk',
        language: 'en', // language of the results
        types: '(cities)' // default: 'geocode'
      }}

      styles={{
        textInputContainer: {width: '100%'},
        description: {fontWeight: 'bold'},
        predefinedPlacesDescription: {color: '#1faadb'}
      }}

      currentLocation={false} // Will add a 'Current location' button at the top of the predefined places list
      currentLocationLabel="Current location"
      nearbyPlacesAPI='GooglePlacesSearch' // Which API to use: GoogleReverseGeocoding or GooglePlacesSearch
      // available options for GoogleReverseGeocoding API : https://developers.google.com/maps/documentation/geocoding/intro
      GoogleReverseGeocodingQuery={{}}
      GooglePlacesSearchQuery={{
        // available options for GooglePlacesSearch API : https://developers.google.com/places/web-service/search
        rankby: 'distance',
        // types: 'food'
      }}

      filterReverseGeocodingByTypes={['locality', 'administrative_area_level_3']} // filter the reverse geocoding results by types - ['locality', 'administrative_area_level_3'] if you want to display only cities
      // predefinedPlaces={[homePlace, workPlace]}

      debounce={200} // debounce the requests in ms. Set to 0 to remove debounce. By default 0ms.
      // renderLeftButton={()  => <Text>MM</Text>}
      // renderRightButton={() => <Text>Custom text after the input</Text>}
      />
    );
  }

  return (
    <Container style={common_styles.container}>
    <Header androidStatusBarColor="#9CDEE6" style={common_styles.bgColor}>
    <Left>
    <Button
    transparent
    onPress={() => this.props.navigation.goBack()}
    >
    <Icon name="arrow-back" />
    </Button>
    </Left>
    <Body>
    <Title>Update Profile</Title>
    </Body>
    <Right>
    <Button transparent onPress={() => this.props.navigation.navigate('Notifications')}>
    <Icon style={{ fontSize: 35 }} name="notifications" />
    <Badge style={{ position: 'absolute',marginLeft: 30 }}><Text>2</Text></Badge>
    </Button>
    </Right>
    </Header>

    <Content>


    <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>

    <Form>
    <Item floatingLabel style={{marginTop: 15}}>
    <Label style={common_styles.colorGrey}>First Name</Label>
    <Input onChangeText={(firstName) => this.setState({ firstName })} value={this.state.firstName} />
    </Item>

    <Item floatingLabel style={{marginTop: 15}}>
    <Label style={common_styles.colorGrey}>Last Name</Label>
    <Input onChangeText={(lastName) => this.setState({ lastName })} value={this.state.lastName} />
    </Item>

    <Item floatingLabel style={{marginTop: 15}}>
    <Label style={common_styles.colorGrey}>Email Address</Label>
    <Input onChangeText={(email) => this.setState({ email })} value={this.state.email} />
    </Item>

    <Item floatingLabel style={{marginTop: 15}}>
    <Label style={common_styles.colorGrey}>Phone Number </Label>
    <Input onChangeText={(mobileNo) => this.setState({ mobileNo })} value={this.state.mobileNo} />
    </Item>


    <DatePicker
    style={{marginTop: 15,width:300}}
    date={this.state.date}
    mode="date"
    placeholder="select date"
    format="YYYY-MM-DD"
    minDate="1950-05-01"
    maxDate="2016-06-01"
    confirmBtnText="Confirm"
    cancelBtnText="Cancel"
    customStyles={{
      dateIcon: {
        position: 'absolute',
        left: 0,
        top: 4,
        marginLeft: 0
      },
      dateInput: {
        marginLeft: 36
      }
      // ... You can check the source to find the other keys.
    }}
    onDateChange={(date) => {this.setState({date: date})}}
    />


    <Item floatingLabel style={{marginTop: 15}}>
    <Label style={common_styles.colorGrey}>Occupation</Label>
    <Input onChangeText={(occupation) => this.setState({ occupation })} value={this.state.occupation} />
    </Item>

    <Item floatingLabel style={{marginTop: 15}}>
    <Label style={common_styles.colorGrey}>Website </Label>
    <Input onChangeText={(website) => this.setState({ website })} value={this.state.website} />
    </Item>

    <Item style={{marginTop: 15}}>
    < GooglePlacesInput/>
    </Item>

    <Item style={{marginTop: 15}}>
    <Picker
    selectedValue={this.state.gender}
    style={{height: 50, width: 100}}
    onValueChange={(itemValue, itemIndex) =>
      this.setState({gender: itemValue})
    }>
    <Picker.Item label="Male" value="M" />
    <Picker.Item label="Female" value="F" />
    </Picker>
    </Item>
    <Button block rounded
    style={common_styles.buttonT25}
    onPress={this.UpdateProfileAction}>
    <Text>Update Profile</Text>
    </Button>

    </Form>

    </View>
    </Content>
    </Container>
  );
}
}

export default UpdateProfile;
