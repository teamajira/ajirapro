import React, { Component } from "react";
import {
  Container,
  Header,
  Title,
  Content,
  Button,
  Badge,
  Icon,
  Left,
  Right,
  Body,
  Text,
 Input,
 Item, Form, Label
} from "native-base";

import { NetInfo,AsyncStorage,StyleSheet, View, PixelRatio, TouchableOpacity, Image, Alert } from 'react-native';

import ImagePicker from 'react-native-image-picker';

// import RNFetchBlob from 'rn-fetch-blob';

import common_styles from "../../common_styles";

import FormData from 'FormData';
import CryptoJS from 'crypto-js';
import DeviceInfo from 'react-native-device-info';
import DialogProgress from 'react-native-dialog-progress';

import { RAND, HASH,API_URL,APP_KEY,APP_VERSION, API_UPLOAD_PICTURE,COLOR_ORANGE, CryptoJS512 }  from "../../common_scripts";

let sha1 = require('sha1');
const options = {
    title:"Loading Data",
    message:"Please wait...",
    isCancelable:true
}

const deviceId = DeviceInfo.getUniqueID();
const osVersion = DeviceInfo.getVersion();

let requestData = null;

class UploadPicture extends React.Component {

  constructor(props){
    super(props);

    this.rand = sha1(new Date().getTime());

    this.currentDate = new Date();
    this.month = this.currentDate.getMonth() + 1;
    this.currentMonth = this.month < 10 ? '0' + this.month : '' + this.month
    this.currentYear = this.currentDate.getFullYear();
    this.dateValue = this.currentMonth.toString() + this.currentYear.toString();

    this.generateHash = function(rand) {
         return APP_VERSION + '' + CryptoJS512(CryptoJS512(rand) + CryptoJS512(APP_KEY) + CryptoJS512(rand + API_UPLOAD_PICTURE + this.dateValue));
    }

    this.hash = this.generateHash(this.rand);
    this.url = API_URL + API_UPLOAD_PICTURE;

    const { navigation } = this.props;

    this.state = {
        jsonData: "",
        token: "",
        fieldTitle: "",
        fieldDescription:"",
        file:"",
        ImageSource: null,
      data: null,
      Image_TAG: ''
      }

   }

     componentDidMount() {

        this._loadData();

     }

     async _loadData () {
       try{
             let response = await AsyncStorage.getItem('loginData');
             let jsonDatas = await JSON.parse(response) || "";

             this.setState({
               "token": jsonDatas.token
             });

             console.log('set Token: ' + this.state.token);

             //this.getProfile();
             //DialogProgress.hide()
           }catch (error) {
            console.log('AsyncStorage error: ' + error.message);
          }
       }

      UploadPictureAction = () =>{

        if(this.state.fieldTitle =='') {
          alert("Please enter title for your photo");
        }
        else if(this.state.fieldDescription=='') {
          alert("Please enter description for your photo");
        }
        else if(this.state.data ==='') {
          alert("Please browse for the photo you want to upload");
        }
        else {

               DialogProgress.show(options);

               let rand = sha1(new Date().getTime());
               let hash = this.generateHash(rand);
               let url = API_URL + API_UPLOAD_PICTURE;

    //            const {iamgeSource} = this.state.ImageSource;
    //            //Add your photo
    // //this, retrive the file extension of your photo
    // const uriPart = iamgeSource.split('.');
    // const fileExtension = uriPart[uriPart.length - 1];

    console.log("last ImageSource---"+this.state.ImageSource)

               const formData = new FormData();
               formData.append('hash', hash);
               formData.append('rand', rand);
               formData.append('uuid', deviceId);
               formData.append('token', this.state.token);
               formData.append('fieldTitle', this.state.fieldTitle);
               formData.append('fieldDescription', this.state.fieldDescription);
               // formData.append('Content-Type',"multipart/formdata");
               formData.append('file', {
                       uri: this.state.file.source,
                       type: 'image/jpg',
                       name: (this.state.file.source).split(/[/ ]+/).pop()
                     });

               let postData = {
                 method: 'POST',
                 headers: {
                     'Accept': 'application/json',
                     'Content-Type': 'multipart/form-data'
                 },
                 body:formData
               }

               console.log("UploadPicture postdata:"+ JSON.stringify(postData));

               NetInfo.isConnected.fetch().then((isConnected) => {
                 if(isConnected){
               fetch(url, postData)
                  .then(response => {
                    console.log('UploadPicture 1 response---'+JSON.stringify(response));
                    return response.json()
                  })
                  .then((responseJson) => {
                    console.log("UploadPicture 2 response:"+ JSON.stringify(responseJson));

                    if(responseJson.settings.success){
                       alert('Your photo was uploaded successfully');

                       // let navigateVerify = NavigationActions.navigate({
                       //     routeName: 'Profile'
                       // });
                       // this.props.navigation.dispatch(navigateVerify);

                    }
                    else {
                      alert(responseJson.settings.message);
                    }

               })
               .catch((error) =>{
                 console.log('Erroor---'+JSON.stringify(error));
                   if(error == 'TypeError: Network request failed'){
                       alert('Kindly check if the device is connected to stable cellular data connection or WiFi.');
                   }
               })

               DialogProgress.hide();
             }else{
                DialogProgress.hide();
                alert("Kindly check if the device is connected to stable cellular data connection or WiFi.");
             }
           }).catch((error)=>{
             DialogProgress.hide();
              console.log("Api call error");
              alert('API CALL error----'+error.message);
           }).done();

         }
        }

//         handelChangeImage(avatar) {
//   //const reader = new FileReader();
//   //reader.onload = function(ee) {
//     console.log("Selected Image---"+avatar);
//     this.setState({file: avatar});
//   ///}.bind(this);
// }


selectPhotoTapped() {
    const options = {
      quality: 1.0,
      maxWidth: 500,
      maxHeight: 500,
      storageOptions: {
        skipBackup: true
      }
    };

    ImagePicker.showImagePicker(options, (response) => {
      console.log('Response = ', response);

      if (response.didCancel) {
        console.log('User cancelled photo picker');
      }
      else if (response.error) {
        console.log('ImagePicker Error: ', response.error);
      }
      else if (response.customButton) {
        console.log('User tapped custom button: ', response.customButton);
      }
      else {
        let source = { uri: response.uri };
        let image = {
            'source': response.uri
        };

        //console.log('IMAGE source---'+JSON.stringify(source.uri));

        this.setState({

          file:image,
          ImageSource: source,
          data: response.data

        });
      }
    });
  }

  // uploadImageToServer = () => {
  //
  //   RNFetchBlob.fetch('POST', 'http://192.168.2.102/Project/upload_image.php', {
  //     Authorization: "Bearer access-token",
  //     otherHeader: "foo",
  //     'Content-Type': 'multipart/form-data',
  //   }, [
  //       { name: 'image', filename: 'image.png', type: 'image/png', data: this.state.data },
  //       { name: 'image_tag', data: this.state.Image_TAG }
  //     ]).then((resp) => {
  //
  //       var tempMSG = resp.data;
  //
  //       tempMSG = tempMSG.replace(/^"|"$/g, '');
  //
  //       Alert.alert(tempMSG);
  //
  //     }).catch((err) => {
  //       // ...
  //     })
  //
  // }


  render() {
    return (
      <Container style={common_styles.container}>
        <Header androidStatusBarColor="#9CDEE6" style={common_styles.bgColor}>
          <Left>
            <Button
              transparent
              onPress={() => this.props.navigation.goBack()}
            >
              <Icon name="arrow-back" />
            </Button>
          </Left>
          <Body>
            <Title>Upload Picture</Title>
          </Body>
          <Right>
            <Button transparent onPress={() => this.props.navigation.navigate('Notifications')}>
              <Icon style={{ fontSize: 35 }} name="notifications" />
              <Badge style={{ position: 'absolute',marginLeft: 30 }}><Text>2</Text></Badge>
            </Button>
          </Right>
        </Header>

        <Content padder>
        <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>


       <Item floatingLabel style={{marginTop: 15}}>
       <Label style={common_styles.colorGrey}>Photo Title</Label>
       <Input onChangeText={(fieldTitle) => this.setState({ fieldTitle })}/>
       </Item>

       <Item floatingLabel style={{marginTop: 15}}>
       <Label style={common_styles.colorGrey}>Photo Description</Label>
       <Input onChangeText={(fieldDescription) => this.setState({ fieldDescription })}/>
       </Item>

       <TouchableOpacity onPress={this.selectPhotoTapped.bind(this)}>

                 <View style={styles.ImageContainer}>

                   {this.state.ImageSource === null ? <Text>Select a Photo</Text> :
                     <Image style={styles.ImageContainer} source={this.state.ImageSource} />
                   }

                 </View>

               </TouchableOpacity>



        <TouchableOpacity onPress={this.UploadPictureAction} activeOpacity={0.6} style={styles.button} >

          <Text style={styles.TextStyle}> UPLOAD IMAGE TO SERVER </Text>

        </TouchableOpacity>


            <Button block rounded
                    style={common_styles.buttonT25}
                    onPress={this.UploadPictureAction}>
              <Text>Save Photo</Text>
            </Button>


              </View>
        </Content>
      </Container>
    );
  }
}



const styles = StyleSheet.create({

  ImageContainer: {
    borderRadius: 10,
    width: 250,
    height: 250,
    borderColor: '#9B9B9B',
    borderWidth: 1 / PixelRatio.get(),
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#CDDC39',
    marginTop:20,
  },

  button: {

    width: '80%',
    backgroundColor: '#00BCD4',
    borderRadius: 7,
    marginTop: 20
  },

  TextStyle: {
    color: '#fff',
    textAlign: 'center',
    padding: 10
  }

});

export default UploadPicture;
