import React, { Component } from "react";
import Icon from 'react-native-vector-icons/MaterialIcons';
import { Content,ListItem,Badge,Text,Left,Right,Body } from "native-base";

import c_styles from "AjiraPro/src/screens/common_styles";

import { withNavigation } from 'react-navigation';

export const COLOR_GREEN = "#008000";
export const COLOR_RED = "#ff0000";

class Rating extends React.Component {

  constructor(props){
    super(props);

    this.state = {
        quoteDetails: ""
      }
   }

  componentDidMount () {

    this.setState({
      //quoteDetails: this.props.screenProps.quoteData
    })
  }

  render() {

const mydata = this.props.screenProps;

    return (
      <Content>
          <ListItem>
            <Icon name="trending-up" size={30} color="green"/>
            <Body><Text> Number of times rated</Text></Body>
            <Right>
              <Badge success
                textStyle={{ color: "white" }}>
                <Text>{mydata.ratingData.noOfTimesHired}</Text>
              </Badge>
            </Right>
          </ListItem>

          <ListItem last>
            <Icon name="stars" size={30} color="orange"/>
            <Body><Text> Average Rating</Text></Body>
            <Right>
              <Badge warning
                textStyle={{ color: "white" }}>
                <Text>{mydata.ratingData.rating}</Text>
              </Badge>
            </Right>
          </ListItem>
      </Content>
    );
  }
}

export default withNavigation(Rating);
