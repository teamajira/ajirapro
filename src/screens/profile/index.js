import React, { Component } from "react";
import {ActivityIndicator,AsyncStorage,NetInfo,Image,View } from "react-native";
import {Container,Header,Title,Button,Icon,Tabs,Tab,TabHeading,Right,Left,Body,ScrollableTab,Badge,Text,Thumbnail,Content,H2} from "native-base";

/*Import required tabs*/
import Info from "./info";
import Services from "./services";
import Rating from "./rating";

/*Import formData*/
import FormData from 'FormData';
import CryptoJS from 'crypto-js';
import DeviceInfo from 'react-native-device-info';
import DialogProgress from 'react-native-dialog-progress';

/*Import specific styles for profile page*/
import styles from "./styles";
import common_styles from "../common_styles";

/*Import required API related functions*/
import { HASH,API_URL,APP_KEY,APP_VERSION, API_PROFILE,COLOR_ORANGE,COLOR_TURGUOISE }  from "../common_scripts";

let sha1 = require('sha1');

/*Spinner dialog options*/
const options = {
    title:"Loading Data",
    message:"Please wait...",
    isCancelable:true
}

const deviceId = DeviceInfo.getUniqueID();
const osVersion = DeviceInfo.getVersion();

// const profileBanner = require("../../../assets/profile_banner.png");

const tabsUnder = {
  tabBarUnderlineStyle: { backgroundColor: '#9CDEE6' }
}

export default class Profile extends Component {

  constructor(props){
    super(props);

    this.state = {
      professionalData:"",
      professionaltopInfo:"",
      professionalpersonalInfo: "",
      professionalAbout: "",
      professionalServices:"",
      professionalPictures:"",
      token: "",
      isLoading: ""
    }
  }

  componentDidMount() {

    this._loadData();

  }

  async _loadData () {
    try{
      let response = await AsyncStorage.getItem('loginToken');

      this.setState({
        token: response
      });

      this.getProfile();
    }catch (error) {
      this.setState({isLoading: false});
      console.log('AsyncStorage error: ' + error.message);
    }
  }


  getProfile = () =>{

    DialogProgress.show(options);

    let rand = sha1(new Date().getTime());
    let url = API_URL + API_PROFILE;

    const formData = new FormData();
    formData.append('hash', HASH(API_PROFILE,rand));
    formData.append('rand', rand);
    formData.append('uuid', deviceId);
    formData.append('token', this.state.token);

    let postData = {
      method: 'POST',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'multipart/form-data'
      },
      body:formData
    }

    //console.log("profile postdata:"+ JSON.stringify(postData));

    fetch(url, postData)
    .then(response => {
      return response.json()
    })
    .then((responseJson) => {
      console.log("profile response:"+ JSON.stringify(responseJson));
      this.setState({isLoading: false});

      if(responseJson.settings.success){
        if(responseJson.data){

          professionaltopInfo = responseJson.data.profile.topInfo;
          professionalpersonalInfo = responseJson.data.profile.personalInfo;
          professionalAbout = responseJson.data.profile.about;
          professionalServices = responseJson.data.profile.services;
          professionalPictures = responseJson.data.profile.pictures;
          professionalData = JSON.stringify(responseJson.data);
        }

        this.setState({
          professionaltopInfo: professionaltopInfo,
          professionalpersonalInfo: professionalpersonalInfo,
          professionalAbout: professionalAbout,
          professionalServices: professionalServices,
          professionalData: professionalData,
          professionalPictures:professionalPictures,
        });

      }
    })
    .catch((error) =>{
      if(error == 'TypeError: Network request failed'){
        alert('Kindly check if the device is connected to stable cellular data connection or WiFi.',);
        this.setState({isLoading: false});
      }
    })

    DialogProgress.hide();
  }

  _toUpper(str) {

    if(str && str !== null){
      return str .toLowerCase()
      .split(' ')
      .map(function(word) {
        return word[0].toUpperCase() + word.substr(1);
      })
      .join(' ');
    }
  }

  _displayProfile(customerName){
    if(!this.state.professionalpersonalInfo && !this.state.isLoading){
          return <ActivityIndicator color={COLOR_TURGUOISE} size={40} style={{marginTop:100}}/>
     }else{
       return (
         <Content>
           <View style={{backgroundColor: COLOR_TURGUOISE, height: 250 }}>
             <View style={{ height: 150 }} >
             <Text style={{textAlign:"center"}}>
               <H2 style={{color:"white"}}> {this._toUpper(customerName)}</H2>
               </Text>
               <Icon name="pin" style={styles.profileDetails}>
                 <Text style={{color:"white"}}> {this.state.professionalpersonalInfo.address}</Text>
               </Icon>

               <View style={{flexDirection: "row"}}>
               {/*
                 <Text style={{textAlign:"right",color: "white",fontSize: 25, marginRight: 5}}>
                   3 <Icon name="star" style={{color:"white"}}/>
                 </Text>*/}

                 <Left style={{marginLeft: 10}}>
                   <Text style={{color: "white",fontSize: 25, marginLeft: 32}}>
                   {this.state.professionaltopInfo.noOfTimesHired}
                   </Text>
                   <Text style={common_styles.text}>Times Hired</Text>
                 </Left>

                 <Right style={{marginRight: 30,marginBottom: 30}}>
                   <Text style={{textAlign:"right",color: "white",fontSize: 25, marginRight: 5}}>
                     {this.state.professionaltopInfo.rating} <Icon name="star" style={{color:"white"}}/>
                   </Text>
                 </Right>

               </View>
             </View>

             <View  style={styles.imageHolder}>
                 <Thumbnail large source={{uri:this.state.professionaltopInfo.personalPhoto}} style={styles.profileImage} />
             </View>
           </View>

           <Tabs {... tabsUnder}  renderTabBar={() => <ScrollableTab style={{backgroundColor: "transparent"}} />}  >
             <Tab heading={
               <TabHeading style={{backgroundColor: "transparent"}}>
                   <Text style={common_styles.textTheme}>Info</Text>
               </TabHeading>}>
               <Info screenProps={{ topData: this.state.professionaltopInfo, personalData: this.state.professionalpersonalInfo,aboutData: this.state.professionalAbout }}/>
             </Tab>

             <Tab heading={
               <TabHeading style={{backgroundColor: "transparent"}} >
                   <Text style={common_styles.textTheme}>Services</Text>
               </TabHeading>}>
               <Services screenProps={{ servicesData: this.state.professionalServices, picturesData: this.state.professionalPictures }}/>
             </Tab>

             <Tab heading={
               <TabHeading  style={{backgroundColor: "transparent"}}>
                   <Text style={common_styles.textTheme}>Rating</Text>
               </TabHeading>}>
               <Rating screenProps={{ ratingData: this.state.professionaltopInfo }}/>
             </Tab>
           </Tabs>
         </Content>
       );
     }
  }

  render() {

    var customerName = this.state.professionaltopInfo.firstName+" "+this.state.professionaltopInfo.lastName
    return (
      <Container >
          <Header hasTabs androidStatusBarColor="#9CDEE6" style={common_styles.bgColor}>
            <Left>
            <Button
              transparent
              onPress={() => this.props.navigation.navigate("DrawerOpen")}>
              <Icon name="menu" />
            </Button>
            </Left>
            <Body>
            <Title>Profile</Title>
            </Body>
            <Right>
              <Button transparent onPress={() => this.props.navigation.navigate('Notifications')}>
                <Icon style={{ fontSize: 35 }} name="notifications" />
                <Badge style={{ position: 'absolute',marginLeft: 30 }}><Text>2</Text></Badge>
              </Button>
            </Right>
          </Header>
          {this._displayProfile(customerName)}
      </Container>
    );
  }
}
