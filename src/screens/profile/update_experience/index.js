import React, { Component } from "react";
import {
  Container,
  Header,
  Title,
  Content,
  Button,
  Badge,
  Icon,
  Left,
  Right,
  Body,
  Text,
 Input,
 Item, Form, Label
} from "native-base";
import { View, NetInfo,AsyncStorage } from "react-native";
import common_styles from "../../common_styles";

import FormData from 'FormData';
import CryptoJS from 'crypto-js';
import DeviceInfo from 'react-native-device-info';
import DialogProgress from 'react-native-dialog-progress';

import { RAND, HASH,API_URL,APP_KEY,APP_VERSION, API_UPDATE_ABOUT,COLOR_ORANGE, CryptoJS512 }  from "../../common_scripts";

let sha1 = require('sha1');
const options = {
    title:"Loading Data",
    message:"Please wait...",
    isCancelable:true
}

const deviceId = DeviceInfo.getUniqueID();
const osVersion = DeviceInfo.getVersion();

let requestData = null;

class UpdateExperience extends React.Component {

  constructor(props){
    super(props);

    this.rand = sha1(new Date().getTime());

    this.currentDate = new Date();
    this.month = this.currentDate.getMonth() + 1;
    this.currentMonth = this.month < 10 ? '0' + this.month : '' + this.month
    this.currentYear = this.currentDate.getFullYear();
    this.dateValue = this.currentMonth.toString() + this.currentYear.toString();

    this.generateHash = function(rand) {
         return APP_VERSION + '' + CryptoJS512(CryptoJS512(rand) + CryptoJS512(APP_KEY) + CryptoJS512(rand + API_UPDATE_ABOUT + this.dateValue));
    }

    this.hash = this.generateHash(this.rand);
    this.url = API_URL + API_UPDATE_ABOUT;

    const { navigation } = this.props;

    let experience = navigation.getParam('experience', '');

    this.state = {
        jsonData: "",
        token: "",
        experience: experience,
      }

   }

     componentDidMount() {

        this._loadData();

     }

     async _loadData () {
       try{
             let response = await AsyncStorage.getItem('loginData');
             let jsonDatas = await JSON.parse(response) || "";

             this.setState({
               "token": jsonDatas.token
             });

             console.log('set Token: ' + this.state.token);

             //this.getProfile();
             //DialogProgress.hide()
           }catch (error) {
            console.log('AsyncStorage error: ' + error.message);
          }
       }

      UpdateExperienceAction = () =>{

        if(this.state.experience ==null) {
          alert("Please enter brief description experience you");
        }
        else {

               DialogProgress.show(options);

               let rand = sha1(new Date().getTime());
               let hash = this.generateHash(rand);
               let url = API_URL + API_UPDATE_ABOUT;

               const formData = new FormData();
               formData.append('hash', hash);
               formData.append('rand', rand);
               formData.append('uuid', deviceId);
               formData.append('token', this.state.token);
               formData.append('experience', this.state.experience);

               let postData = {
                 method: 'POST',
                 headers: {
                     'Accept': 'application/json',
                     'Content-Type': 'multipart/form-data'
                 },
                 body:formData
               }

               console.log("UpdateExperience postdata:"+ JSON.stringify(postData));

               NetInfo.isConnected.fetch().then((isConnected) => {
                 if(isConnected){
               fetch(url, postData)
                  .then(response => {
                    console.log('response---'+JSON.stringify(response));
                    return response.json()
                  })
                  .then((responseJson) => {
                    console.log("UpdateExperience response:"+ JSON.stringify(responseJson));

                    if(responseJson.settings.success){
                       alert('Experience details were updated successfully');

                       // let navigateVerify = NavigationActions.navigate({
                       //     routeName: 'Profile'
                       // });
                       // this.props.navigation.dispatch(navigateVerify);

                    }
                    else {
                      alert(responseJson.settings.message);
                    }

               })
               .catch((error) =>{
                 console.log('Erroor---'+JSON.stringify(error));
                   if(error == 'TypeError: Network request failed'){
                       alert('Kindly check if the device is connected to stable cellular data connection or WiFi.');
                   }
               })

               DialogProgress.hide();
             }else{
                DialogProgress.hide()
                alert("Kindly check if the device is connected to stable cellular data connection or WiFi.");
             }
           }).catch((error)=>{
              console.log("Api call error");
              alert(error.message);
           }).done();

         }
        }

  render() {
    return (
      <Container style={common_styles.container}>
        <Header androidStatusBarColor="#9CDEE6" style={common_styles.bgColor}>
          <Left>
            <Button
              transparent
              onPress={() => this.props.navigation.goBack()}
            >
              <Icon name="arrow-back" />
            </Button>
          </Left>
          <Body>
            <Title>Update Experience</Title>
          </Body>
          <Right>
            <Button transparent onPress={() => this.props.navigation.navigate('Notifications')}>
              <Icon style={{ fontSize: 35 }} name="notifications" />
              <Badge style={{ position: 'absolute',marginLeft: 30 }}><Text>2</Text></Badge>
            </Button>
          </Right>
        </Header>

        <Content padder>
        <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>

       <Item floatingLabel style={{marginTop: 15}}>
       <Label style={common_styles.colorGrey}>Experience</Label>
       <Input value={this.state.experience}
       multiline={true}
       numberOfLines={6}
       style={{ height:150, textAlignVertical: 'top',}}
       onChangeText={(experience) => this.setState({ experience })}/>
       </Item>

            <Button block rounded
                    style={common_styles.buttonT25}
                    onPress={this.UpdateExperienceAction}>
              <Text>Update Experience</Text>
            </Button>

              </View>
        </Content>
      </Container>
    );
  }
}

export default UpdateExperience;
