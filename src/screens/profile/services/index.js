import React, { Component } from "react";
import { Header,Title,Content, Badge, List, ListItem, Text, Button, Icon, Left, Right, Body, Thumbnail } from "native-base";
import {Modal,TouchableHighlight,AsyncStorage, FlatList, StyleSheet, View, Image, Alert, NetInfo } from "react-native";

import { withNavigation } from 'react-navigation';
import styles from "./../styles";
import c_styles from "../../common_styles";
import common_styles from "../../common_styles";

import FormData from 'FormData';
import CryptoJS from 'crypto-js';
import DeviceInfo from 'react-native-device-info';
import DialogProgress from 'react-native-dialog-progress';

import { RAND, HASH,API_URL,APP_KEY,APP_VERSION, API_DELETE_PICTURE,COLOR_ORANGE, CryptoJS512 }  from "../../common_scripts";

let sha1 = require('sha1');
const options = {
    title:"Loading Data",
    message:"Please wait...",
    isCancelable:true
}

const deviceId = DeviceInfo.getUniqueID();
const osVersion = DeviceInfo.getVersion();

let requestData = null;

class Services extends React.Component {

  constructor(props){
    super(props);

    this.rand = sha1(new Date().getTime());

    this.currentDate = new Date();
    this.month = this.currentDate.getMonth() + 1;
    this.currentMonth = this.month < 10 ? '0' + this.month : '' + this.month
    this.currentYear = this.currentDate.getFullYear();
    this.dateValue = this.currentMonth.toString() + this.currentYear.toString();

    this.generateHash = function(rand) {
         return APP_VERSION + '' + CryptoJS512(CryptoJS512(rand) + CryptoJS512(APP_KEY) + CryptoJS512(rand + API_DELETE_PICTURE + this.dateValue));
    }

    this.hash = this.generateHash(this.rand);
    this.url = API_URL + API_DELETE_PICTURE;

    this.state = {
        jsonData: "",
        modalVisible: false,
        modalProduct:'',
        modalTitleText:'',
        pictureId:'',
        token: ""
      }
  }

  setModalVisible(visible,modalProduct,modalTitleText) {
    this.setState({modalVisible: visible,modalProduct:modalProduct,modalTitleText:modalTitleText});
  }


  componentDidMount() {

     this._loadData();

  }

  async _loadData () {
    try{
          let response = await AsyncStorage.getItem('loginData');
          let jsonDatas = await JSON.parse(response) || "";

          this.setState({
            "token": jsonDatas.token
          });

          console.log('set Token: ' + this.state.token);

          //this.getProfile();
          //DialogProgress.hide()
        }catch (error) {
         console.log('AsyncStorage error: ' + error.message);
       }
    }


    ActualDeletePicture(pictureId) {

             DialogProgress.show(options);

             let rand = sha1(new Date().getTime());
             let hash = this.generateHash(rand);
             let url = API_URL + API_DELETE_PICTURE;

             const formData = new FormData();
             formData.append('hash', hash);
             formData.append('rand', rand);
             formData.append('uuid', deviceId);
             formData.append('token', this.state.token);
             formData.append('pictureId', this.state.pictureId);

             let postData = {
               method: 'POST',
               headers: {
                   'Accept': 'application/json',
                   'Content-Type': 'multipart/form-data'
               },
               body:formData
             }

             console.log("DeletePictureById postdata:"+ JSON.stringify(postData));

             NetInfo.isConnected.fetch().then((isConnected) => {
               if(isConnected){
             fetch(url, postData)
                .then(response => {
                  console.log('response---'+JSON.stringify(response));
                  return response.json()
                })
                .then((responseJson) => {
                  console.log("DeletePictureById response:"+ JSON.stringify(responseJson));

                  if(responseJson.settings.success){
                    this.forceUpdate();
                     alert('Your picture was deleted successfully');

                     // let navigateVerify = NavigationActions.navigate({
                     //     routeName: 'Profile'
                     // });
                     // this.props.navigation.dispatch(navigateVerify);

                  }
                  else {
                    alert(responseJson.settings.message);
                  }

             })
             .catch((error) =>{
               console.log('Erroor---'+JSON.stringify(error));
                 if(error == 'TypeError: Network request failed'){
                     alert('Kindly check if the device is connected to stable cellular data connection or WiFi.');
                 }
             })

             DialogProgress.hide();
           }else{
              DialogProgress.hide()
              alert("Kindly check if the device is connected to stable cellular data connection or WiFi.");
           }
         }).catch((error)=>{
            console.log("Api call error");
            alert(error.message);
         }).done();

       //}
      }


  deletePicture(pictureId) {
    this.setState({pictureId: pictureId});
  console.log("Picture....101-----:"+pictureId);
    // Works on both iOS and Android
  Alert.alert(
    'Delete Picture',
    'Are you sure you want to delete this picture?',
    [
      {
        text: 'Cancel',
        onPress: () => console.log('Cancel Pressed'),
        style: 'cancel',
      },
      {text: 'YES', onPress: () => this.ActualDeletePicture(pictureId)},
    ],
    {cancelable: false},
  );

  }

  // ActualDeletePicture(pictureId) {
  //   console.log("Deleting-Picture-----:"+pictureId);
  // }

  _displayData(serviceData){
console.log("services DDD:"+ JSON.stringify(serviceData));
    if(serviceData){
      return <List
          dataArray={serviceData}
          renderRow={data =>
            <ListItem>
              <Body>
                <Text numberOfLines={1} note>
                  {data.serviceSubCategoryName}
                </Text>
              </Body>
            </ListItem>}
          />
    }else{
    return <Text style={c_styles.alignCenter}> You currently have no services.</Text>
    }
  }



  _modalData(modalData){

      return <Modal
                animationType="slide"
                transparent={false}
                visible={this.state.modalVisible}
                onRequestClose={() => {
                  alert('Modal has been closed.');
                }}>
                <View>

                  <View style={styles.ModalInsideView}>
                  <Header hasTabs androidStatusBarColor="#9CDEE6" style={common_styles.bgColor}>
                    <Body>
                    <Title>{this.state.modalTitleText}</Title>
                    </Body>
                    <Right>
                      <Button transparent onPress={() => {
                        this.setModalVisible(!this.state.modalVisible);
                      }}>
                        <Icon style={{ fontSize: 35 }} name="close" />
                      </Button>
                    </Right>
                  </Header>

                    <View style={{ flex: 1, flexDirection: 'column', margin: 1 }}>
                      <Image style={page_styles.imageThumbnail} source={{ uri: this.state.modalProduct }} />
                      </View>

                  </View>
                </View>
              </Modal>

  }


//   _displayPicturesData(picturesData){
// console.log("pictures DDD:"+ JSON.stringify(picturesData));
//     if(picturesData){
//       return <List
//           dataArray={picturesData}
//           renderRow={data =>
//             <Thumbnail style={{justifyContent: 'flex-start',flexDirection: 'column'}} square large source={{uri: data.pictureFile}} />
//             }
//           />
//     }else{
//     return <Text style={c_styles.alignCenter}>You currently have no pictures.</Text>
//     }
//   }

_displayPicturesData(picturesData){
console.log("pictures DDD:"+ JSON.stringify(picturesData));
  if(picturesData){
    return <View style={page_styles.MainContainer}>
        <FlatList
          data={picturesData}
          renderItem={({ item }) => (
            <View style={{ flex: 1, flexDirection: 'column', margin: 1 }}>
              <Image style={page_styles.imageThumbnail} source={{ uri: item.pictureFile }} />
              <ListItem>
                <Left><Text>Title</Text></Left>
                <Body><Text>{item.title}</Text></Body>
              </ListItem>
              <ListItem>
                <Left><Text>Description</Text></Left>
                <Body><Text>{item.description}</Text></Body>
              </ListItem>
              <ListItem>
                <Left>
                <Button transparent onPress={() => {
            this.setModalVisible(true,item.pictureFile,item.title);
          }}>
                <Icon active name="eye" style={styles.profileinfoIcons}/>
                <Text>View</Text>
                </Button>

                </Left>

                <Body>
                <Button transparent onPress={() => {
                  this.deletePicture(item.id);
                }}>
                <Icon active name="close" style={styles.profileinfoIcons}/>
                <Text>Delete</Text>
                </Button>

                </Body>
              </ListItem>
            </View>
          )}
          //Setting the number of column
          numColumns={1}
          keyExtractor={(item, index) => index}
        />

      </View>
  }else{
  return <Text style={c_styles.alignCenter}>You currently have no pictures.</Text>
  }
}

  render() {

    const mydata = this.props.screenProps;

    return (
      <Content padder>
      <ListItem>
        <Text>No. of services Completed - {mydata.servicesData.noOfServicesCompleted}</Text>
      </ListItem>

      <ListItem>
        <Text>No. of Times Hired - {mydata.servicesData.noOfTimesHired}</Text>
      </ListItem>

      <ListItem itemDivider icon>
      <Body>
      <Text>Services Offered</Text>
      </Body>
      </ListItem>


      {this._displayData(mydata.servicesData.services)}

      <ListItem itemDivider icon>
      <Body>
      <Text>Pictures</Text>
      </Body>
      <Right>
      <Button transparent onPress={() => {
        /* 1. Navigate to the DeletePictureById route with params */
        this.props.navigation.navigate('UploadPicture', {
          // experience: mydata.aboutData.experience,
        });
        }}>
      <Icon active name="add-circle" style={styles.profileinfoIcons}/>
      </Button>
      </Right>
      </ListItem>
      <Text style={c_styles.alignCenter}>Upload pictures to showcase your work to clients</Text>
        {this._displayPicturesData(mydata.picturesData)}

        {this._modalData(mydata.picturesData)}

      </Content>
    );
  }
}

export default withNavigation(Services);

const page_styles = StyleSheet.create({
  MainContainer: {
    justifyContent: 'center',
    flex: 1,
  },
  ModalInsideView:{
  justifyContent: 'center',
  alignItems: 'center',
  backgroundColor : "#00BCD4",
  height: 300 ,
  width: '90%',
  borderRadius:10,
  borderWidth: 1,
  borderColor: '#fff'

},
  imageThumbnail: {
    justifyContent: 'center',
    alignItems: 'center',
    height: 200,
  },
});
