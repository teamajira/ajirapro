const React = require("react-native");
const { StyleSheet, Dimensions, Platform } = React;
const deviceHeight = Dimensions.get("window").height;

export default {
  imageHolder:{
    backgroundColor: "#FFF",
    height: 100 ,
    justifyContent: 'center',
    alignItems: "center"
  },
  profileImage: {
    marginTop: -100,
    height: 125,
    width: 125,
    borderRadius: 125/2,
    right: Platform.OS === "android" ? 10 : 50
  },
  profileDetails: {
    textAlign:"center",
    justifyContent: "center",
    color:"white",
    marginLeft: -10
  },
  profileinfoIcons: {
    color:"#FF901E",
    fontSize:30,
  },
  listcontainer: {
    flex: 1,
    flexDirection: 'row',
    borderBottomColor: '#EEE',
    borderBottomWidth: 1,
    marginBottom: 30,
  },
  listfirstwidth: {
    width: '40%',
    alignSelf: 'auto',
  },
  listsecondwidth: {
    width: '60%',
  }
 };
