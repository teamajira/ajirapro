import React, { Component } from "react";
import { Content, List, ListItem, Text, Button, Icon, Left, Right, Body } from "native-base";
import {AsyncStorage, View } from "react-native";

import { withNavigation } from 'react-navigation';
import styles from "./../styles";

class Info extends React.Component {

  constructor(props){
    super(props);
    // this.state = {
    //     professionalpersona: "",
    //     loaded: 'false',
    //   }
  }

  // componentDidMount() {
  //
  //    //this._loadData().done();
  //     //let requestData = props.screenProps;
  //     //console.log(this.props.passedVal);
  //
  //    this.setState({
  //        professionalpersona: this.props.screenProps
  //    });
  //
  // }

  render() {

const mydata = this.props.screenProps;

console.log('INFO dob-----'+mydata.personalData.dob);

    return (
      <Content padder>
      <List>
                  <ListItem itemDivider icon>
                  <Body>
                  <Text>Personal</Text>
                  </Body>
                  <Right>
                  <Button transparent onPress={() => {
                    /* 1. Navigate to the UpdateProfile route with params */
                    this.props.navigation.navigate('UpdateProfile', {
                      itemId: 86,
                      email_address: mydata.personalData.email,
                      mobileNo: mydata.personalData.mobileNo,
                      location: mydata.personalData.location,
                      address: mydata.personalData.address,
                      occupation: mydata.personalData.occupation,
                      firstName: mydata.topData.firstName,
                      lastName: mydata.topData.lastName,
                      dob: mydata.personalData.dob,
                      gender: mydata.personalData.gender,
                      zip: mydata.personalData.zip,
                      city: mydata.personalData.city,
                      countryCode: mydata.personalData.countryCode,
                      state: mydata.personalData.state,
                      lat: mydata.personalData.lat,
                      lon: mydata.personalData.lon,
                    });
                    }}>
                  <Icon active name="create" style={styles.profileinfoIcons}/>
                  </Button>
                  </Right>
                  </ListItem>


                  <View style={styles.listcontainer}>
                    <View style={styles.listfirstwidth}>
                    <Text>Email</Text>
                    </View>
                    <View style={styles.listsecondwidth}>
                    <Text>{mydata.personalData.email}</Text>
                    </View>
                  </View>

                  <View style={styles.listcontainer}>
                    <View style={styles.listfirstwidth}>
                    <Text>Phone</Text>
                    </View>
                    <View style={styles.listsecondwidth}>
                    <Text>{mydata.personalData.mobileNo}</Text>
                    </View>
                  </View>

                  <View style={styles.listcontainer}>
                    <View style={styles.listfirstwidth}>
                    <Text>Date of Birth</Text>
                    </View>
                    <View style={styles.listsecondwidth}>
                    <Text>{mydata.personalData.dob}</Text>
                    </View>
                  </View>

                  <View style={styles.listcontainer}>
                    <View style={styles.listfirstwidth}>
                    <Text>Occupation</Text>
                    </View>
                    <View style={styles.listsecondwidth}>
                    <Text>{mydata.personalData.occupation}</Text>
                    </View>
                  </View>

                  <View style={styles.listcontainer}>
                    <View style={styles.listfirstwidth}>
                    <Text>Gender</Text>
                    </View>
                    <View style={styles.listsecondwidth}>
                    <Text>{mydata.personalData.gender}</Text>
                    </View>
                  </View>

                  <View style={styles.listcontainer}>
                    <View style={styles.listfirstwidth}>
                    <Text>Website</Text>
                    </View>
                    <View style={styles.listsecondwidth}>
                    <Text>{mydata.personalData.website}</Text>
                    </View>
                  </View>

                  <View style={styles.listcontainer}>
                    <View style={styles.listfirstwidth}>
                    <Text>Location</Text>
                    </View>
                    <View style={styles.listsecondwidth}>
                    <Text>{mydata.personalData.address}</Text>
                    </View>
                  </View>



                  <ListItem itemDivider icon>
                  <Body>
                  <Text>Education</Text>
                  </Body>
                  <Right>
                  <Button transparent onPress={() => {
                    /* 1. Navigate to the UpdateAbout route with params */
                    this.props.navigation.navigate('UpdateEducation', {
                      education: mydata.aboutData.education,
                    });
                    }}>
                  <Icon active name="create" style={styles.profileinfoIcons}/>
                  </Button>
                  </Right>
                  </ListItem>
                  <ListItem>
                    <Text>{mydata.aboutData.education}</Text>
                  </ListItem>



                  <ListItem itemDivider icon>
                  <Body>
                  <Text>Experience</Text>
                  </Body>
                  <Right>
                  <Button transparent onPress={() => {
                    /* 1. Navigate to the UpdateAbout route with params */
                    this.props.navigation.navigate('UpdateExperience', {
                      experience: mydata.aboutData.experience,
                    });
                    }}>
                  <Icon active name="create" style={styles.profileinfoIcons}/>
                  </Button>
                  </Right>
                  </ListItem>
                  <ListItem>
                    <Text>{mydata.aboutData.experience}</Text>
                  </ListItem>



                  <ListItem itemDivider icon>
                  <Body>
                  <Text>About</Text>
                  </Body>
                  <Right>
                  <Button transparent onPress={() => {
                    /* 1. Navigate to the UpdateAbout route with params */
                    this.props.navigation.navigate('UpdateAbout', {
                      about: mydata.aboutData.about,
                    });
                    }}>
                  <Icon active name="create" style={styles.profileinfoIcons}/>
                  </Button>
                  </Right>
                  </ListItem>
                  <ListItem>
                    <Text>{mydata.aboutData.about}</Text>
                  </ListItem>
                </List>
      </Content>
    );
  }
}


// withNavigation returns a component that wraps ChildComponent and passes in the
// navigation prop
export default withNavigation(Info);
