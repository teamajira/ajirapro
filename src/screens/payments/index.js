import React, { Component } from "react";
import moment from 'moment';
import {AsyncStorage,NetInfo, View, FlatList,ScrollView, RefreshControl } from "react-native";
import {Container,Header,Title,Content,Button,Badge,Icon,Left,Right,Body,Card,CardItem,Text,ListItem} from "native-base";
import FormData from 'FormData';
import CryptoJS from 'crypto-js';
import DeviceInfo from 'react-native-device-info';

import c_styles from "../common_styles";
import { HASH,API_URL,APP_KEY,APP_VERSION, API_PAYMENT,COLOR_ORANGE }  from "../common_scripts";

let sha1 = require('sha1');

const deviceId = DeviceInfo.getUniqueID();
const osVersion = DeviceInfo.getVersion();

let paymentData = null;

export default class Payments extends Component {

  constructor(props){
    super(props);

    this.state = {
        jsonData: "",
        token: "",
        isLoading:true
      }
   }




 componentDidMount() {
    this._loadData();
 }

 async _loadData () {
   try{
     let response = await AsyncStorage.getItem('loginToken');

     this.setState({token: response});

     this.getPayments();
   }catch (error) {
     this.setState({isLoading: false});
     console.log('AsyncStorage error: ' + error.message);
   }
 }

 _onRefresh = () => {
   this.setState({isLoading: true});
   this.getPayments();
 }

   getPayments = () =>{

     let rand = sha1(new Date().getTime());
     let url = API_URL + API_PAYMENT;

     const formData = new FormData();
     formData.append('hash', HASH(API_PAYMENT,rand));
     formData.append('rand', rand);
     formData.append('uuid', deviceId);
     formData.append('token', this.state.token);

     let postData = {
       method: 'POST',
       headers: {
           'Accept': 'application/json',
           'Content-Type': 'multipart/form-data'
       },
       body:formData
     }

     //console.log("Payments postdata:"+ JSON.stringify(postData));

     fetch(url, postData)
        .then(response => {
          return response.json()
        })
        .then((responseJson) => {
          this.setState({isLoading: false});
          //console.log("payments response:"+ JSON.stringify(responseJson.data.withdrawals.list));

          if(responseJson.settings.success) {
            if(responseJson.data.withdrawals.list){
              paymentData = responseJson.data.withdrawals.list;
            }
          }

          this.setState({
              jsonData: JSON.stringify(responseJson.data, null, 2)
          });
     })
     .catch((error) =>{
         if(error == 'TypeError: Network request failed'){
             alert('Kindly check if the device is connected to stable cellular data connection or WiFi.',);
         }
     })
   }

   _toUpper(str) {
    let value=
        str .toLowerCase()
            .split(' ')
            .map(function(word) {
                return word[0].toUpperCase() + word.substr(1);
            })
            .join(' ');

      if(value == "Completed"){
        return  <View>
                  <Icon name="md-checkmark-circle" style={c_styles.iconList} />
                  <Text style={c_styles.textStatus}>{value}</Text>
                </View>
      }else if(value == "Pending") {
        return <View>
                  <Icon name="md-stopwatch" style={c_styles.iconList} />
                  <Text style={c_styles.textStatus}>{value}</Text>
               </View>
      }else{
        return <Text style={c_styles.textStatus}>{value} </Text>
      }
  }

   _displayPayments(paymentData){
  console.log("Loading "+JSON.stringify(paymentData));

   if(paymentData === null && !this.state.isLoading){
      return <Text style={c_styles.alignCenter}>You currently have no payments.</Text>
    }else{
      return(
        <FlatList
              data={paymentData}
              keyExtractor={item => item.id}
              renderItem={({item}) =>
              <ListItem avatar>
                <Left/>
                <Body>
                  <View>
                    <Text style={c_styles.textName}>{this._toUpper(item.customer.topInfo.firstName+" "+item.customer.topInfo.lastName)}</Text>
                    <Text style={c_styles.textName}>Ksh. {item.amount}</Text>
                    <Text note>Being payment for {item.service.service_category} </Text>
                    <Text note>({item.service.location})</Text>
                    <Text note> {moment(item.service.completed_at).format('Do MMM YYYY hh:mm a')} </Text>
                  </View>
                </Body>
                <Right>
                    <View style={c_styles.listBody}>
                      <View style={c_styles.seperator} />
                      {this._toUpper(item.status)}
                    </View>
                </Right>
              </ListItem>}
            />
      );
    }
  }

  render() {
    return (
      <Container style={c_styles.container}>
        <Header androidStatusBarColor="#9CDEE6" style={c_styles.bgColor}>
          <Left>
            <Button
              transparent
              onPress={() => this.props.navigation.navigate("DrawerOpen")}>
              <Icon name="menu" />
            </Button>
          </Left>
          <Body>
            <Title>Payments</Title>
          </Body>
          <Right>
            <Button transparent onPress={() => this.props.navigation.navigate('Notifications')}>
              <Icon style={{ fontSize: 35 }} name="notifications" />
              <Badge style={{ position: 'absolute',marginLeft: 30 }}><Text>2</Text></Badge>
            </Button>
          </Right>
        </Header>

        <ScrollView
          refreshControl={
            <RefreshControl
              refreshing={this.state.isLoading}
              onRefresh={this._onRefresh}/>
            }>
          <Content>
            <Text>{/*this.state.jsonData*/}</Text>
            {this._displayPayments(paymentData)}
          </Content>
        </ScrollView>
      </Container>
    );
  }
}
