const React = require("react-native");
const { StyleSheet, Dimensions, Platform } = React;
const deviceHeight = Dimensions.get("window").height;

export default {
  cover:{
    width: '100%',
    height: 200
  },
  logoContainer: {
    justifyContent: 'center',
    alignItems: 'center'
  },
  logo: {
    right: Platform.OS === "android" ? 20 : 50,
      marginTop: deviceHeight / 14,
      width: 120,
      height: 100,

  },
 };
