import React, { Component } from "react";
import { ImageBackground, Image } from "react-native";
import {Container,Header,Title,H1,View,Content,Button,Item,Label,Input,Body,Left,Right,Icon,Form,Text} from "native-base";
import common_styles from "../common_styles";
import styles from "./styles";

const banner = require("../../../assets/block.png");
const bannerLogo = require("../../../assets/key.png");

import FormData from 'FormData';
import CryptoJS from 'crypto-js';
import DeviceInfo from 'react-native-device-info';
import DialogProgress from 'react-native-dialog-progress';

import { RAND, HASH,API_URL,APP_KEY,APP_VERSION, API_FORGOT_PASSWORD,COLOR_ORANGE, CryptoJS512 }  from "../common_scripts";

let sha1 = require('sha1');
const options = {
    title:"Loading Data",
    message:"Please wait...",
    isCancelable:true
}

const deviceId = DeviceInfo.getUniqueID();
const osVersion = DeviceInfo.getVersion();

let requestData = null;


class ForgotPassword extends Component {

  constructor(props){
    super(props);

    this.rand = sha1(new Date().getTime());

    this.currentDate = new Date();
    this.month = this.currentDate.getMonth() + 1;
    this.currentMonth = this.month < 10 ? '0' + this.month : '' + this.month
    this.currentYear = this.currentDate.getFullYear();
    this.dateValue = this.currentMonth.toString() + this.currentYear.toString();

    this.generateHash = function(rand) {
         return APP_VERSION + '' + CryptoJS512(CryptoJS512(rand) + CryptoJS512(APP_KEY) + CryptoJS512(rand + API_FORGOT_PASSWORD + this.dateValue));
    }

    this.hash = this.generateHash(this.rand);
    this.url = API_URL + API_FORGOT_PASSWORD;

    this.state = {
        jsonData: "",
        token: "",
        email: "",
      }

   }


   ResetPasswordAction = () =>{

     if(this.state.email =="") {
       alert("Please enter your phone number or email address");
     }
     else {

            DialogProgress.show(options);

            let rand = sha1(new Date().getTime());
            let hash = this.generateHash(rand);
            let url = API_URL + API_FORGOT_PASSWORD;

            const navigateAction = NavigationActions.navigate({
              routeName: 'SetPassword'
            })

            const formData = new FormData();
            formData.append('hash', hash);
            formData.append('rand', rand);
            formData.append('uuid', deviceId);
            formData.append('email', this.state.email);

            let postData = {
              method: 'POST',
              headers: {
                  'Accept': 'application/json',
                  'Content-Type': 'multipart/form-data'
              },
              body:formData
            }

            console.log("ResetPassword postdata:"+ JSON.stringify(postData));

            NetInfo.isConnected.fetch().then((isConnected) => {
              if(isConnected){
            fetch(url, postData)
               .then(response => {
                 console.log('response---'+JSON.stringify(response));
                 return response.json()
               })
               .then((responseJson) => {
                 console.log("ResetPassword response:"+ JSON.stringify(responseJson));

                 if(responseJson.settings.success){
                   AsyncStorage.setItem("otpCodeData", JSON.stringify(responseJson.data));
                   this.props.navigation.dispatch(navigateAction);
                    alert('Password Reset request was successful');
                 }
                 else {
                   alert(responseJson.settings.message);
                 }

            })
            .catch((error) =>{
              console.log('Erroor---'+JSON.stringify(error));
                if(error == 'TypeError: Network request failed'){
                    alert('Kindly check if the device is connected to stable cellular data connection or WiFi.');
                }
            })

            DialogProgress.hide();
          }else{
             DialogProgress.hide()
             alert("Kindly check if the device is connected to stable cellular data connection or WiFi.");
          }
        }).catch((error)=>{
           console.log("Api call error");
           alert(error.message);
        }).done();

      }
    }

  render() {
    return (
      <Container style={common_styles.container}>
        <Header androidStatusBarColor="#9CDEE6" hasSegment style={common_styles.bgColor}>
          <Left>
            <Button transparent onPress={() => this.props.navigation.navigate('Login')}>
              <Icon name="arrow-back" />
            </Button>
          </Left>
          <Body>
            <Title>Forgot Password</Title>
          </Body>
          <Right />
        </Header>

        <Content>
          <ImageBackground source={banner} style={styles.cover} >
            <View style={styles.logoContainer}>
              <Image source={bannerLogo} style={styles.logo} />
            </View>
          </ImageBackground>
          <H1 style={common_styles.alignCenter}> Forgot your password ?</H1>
          <Text style={[common_styles.alignCenter,common_styles.colorGrey]}>Reset Instructions will be sent to your phone number</Text>
          <Form style={common_styles.formSpacing}>
            <Item floatingLabel>
              <Label>Phone Number</Label>
              <Input  style={{marginTop: 10}}
                      keyboardType = "numeric"
                      returnKeyType={ 'done' }
                      onChangeText={(email) => this.setState({ email })}/>
            </Item>
          </Form>

          <Button block rounded style={common_styles.buttonT25} onPress={this.ResetPasswordAction}>
            <Text>Request Password Reset</Text>
          </Button>

          <View >
              <Text style={[common_styles.alignCenter,common_styles.underline]}
              onPress={() => this.props.navigation.navigate('Login')}> Return to Log In
               </Text>
          </View>

        </Content>
      </Container>
    );
  }
}

export default ForgotPassword;
