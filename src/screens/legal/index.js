import React, { Component } from "react";
import { FlatList } from "react-native";
import { NavigationActions } from 'react-navigation';
import {Container,Header,Title,Content,Button,Badge,Icon,Left,Right,Body,Text,Toast,ListItem} from "native-base";
import FormData from 'FormData';
import DeviceInfo from 'react-native-device-info';
import c_styles from "../common_styles";
import { HASH,API_URL,APP_KEY,APP_VERSION, API_LEGAL }  from "AjiraPro/src/screens/common_scripts";


let sha1 = require('sha1');
const deviceId = DeviceInfo.getUniqueID();
const osVersion = DeviceInfo.getVersion();

const legal_list = [
  {
    text: "About Us",
    section:"aboutus"
  },
  {
    text: "Help",
    section:"help"
  },
  {
    text: "Privacy Policy",
    section:"privacypolicy"
  },
  {
    text: "Terms of use",
    section:"terms"
  },
  {
    text: "How It Works",
    section:"howitworks"
  }
];

let content = null;

class Legal extends Component {

  constructor(props){
    super(props);

    this.state = {
        isLoading:true
      }
   }

   componentDidMount () {
     this.legalData();
   }

  legalData = () =>{

     rand = sha1(new Date().getTime());
     url = API_URL + API_LEGAL;

     const formData = new FormData();
     formData.append('hash', HASH(API_LEGAL,rand));
     formData.append('rand', rand);
     formData.append('uuid', deviceId);
     formData.append('osVersion', osVersion);
     let postData = {
       method: 'POST',
       headers: {
           'Accept': 'application/json',
           'Content-Type': 'multipart/form-data'
       },
       body:formData
     }

     fetch(url, postData)
     .then(response => {
       return response.json()
     })
     .then((responseJson) => {
         this.setState({isLoading: false});
         content = responseJson.data;
     })
     .catch((error) =>{
       Toast.show({
         text: "Unable to load legal content",
         textStyle: { color: "red" },
         buttonText: "Dismiss"});
     })

  }


  _legalContent(section){

    if(!this.state.isLoading){
      try{
        const navigateAction = NavigationActions.navigate({
          routeName: "LegalContent",
          params: {
            content: content,
            section: section
          }
        })

        this.props.navigation.dispatch(navigateAction);

      }catch (error) {
        // Error retrieving data
        console.log(error.message);
      }
    }else{
      Toast.show({
        text: "Downloading data, try again",
        textStyle: { color: "orange" },
        buttonText: "Dismiss"});
    }
  }

  render() {
    return (
      <Container style={c_styles.container}>
        <Header androidStatusBarColor="#9CDEE6" style={c_styles.bgColor}>
          <Left>
            <Button
              transparent
              onPress={() => this.props.navigation.navigate("DrawerOpen")}
            >
              <Icon name="menu" />
            </Button>
          </Left>
          <Body>
            <Title>Legal</Title>
          </Body>
          <Right>
            <Button transparent onPress={() => this.props.navigation.navigate('Notifications')}>
              <Icon style={{ fontSize: 35 }} name="notifications" />
              <Badge style={{ position: 'absolute',marginLeft: 30 }}><Text>2</Text></Badge>
            </Button>
          </Right>
        </Header>

        <Content>
          <FlatList
            data={legal_list}
            keyExtractor={(item, index) => "key"+index }
            renderItem={({item}) =>
              <ListItem
                button
                onPress={() => this._legalContent(item.section)}>
                <Left>
                  <Text>
                    {item.text}
                  </Text>
                </Left>
                <Right>
                  <Icon name="arrow-forward" style={{ color: "#999" }} />
                </Right>
              </ListItem>}
          />
        </Content>
      </Container>
    );
  }
}

export default Legal;
