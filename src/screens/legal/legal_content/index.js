import React, { Component } from "react";
import {Container,Header,Title,Content,Button,Badge,Icon,Left,Right,Body,Text} from "native-base";
import { View,Platform,ScrollView } from "react-native";
import HTML from 'react-native-render-html';
import c_styles from "../../common_styles";

export default class LegalContent extends Component {

  constructor(props){

    super(props);

    this.state = {
      title: "",
      content: ""
    }
  }

  componentDidMount() {
    var { params } = this.props.navigation.state;
    legalData = params ? params.content : null
    section = params ? params.section : null

    let title = null;
    let content = null

    console.log(JSON.stringify(legalData.staticpages[0].title));

    if(section === 'aboutus' ){
      title = legalData.staticpages[0].title;
      content = legalData.staticpages[0].content;
    }else if(section === 'help' ){
      title = legalData.staticpages[1].title;
      content = legalData.staticpages[1].content;
    }else if(section === 'privacypolicy' ){
      title = legalData.staticpages[2].title;
      content = legalData.staticpages[2].content;
    }else if(section === 'terms' ){
      title = legalData.staticpages[3].title;
      content = legalData.staticpages[3].content;
    }else if(section === 'howitworks' ){
      title = legalData.staticpages[4].title;
      content = legalData.staticpages[4].content;
    }

    this.setState({
      title: title,
      content: content
    })
  }

  render() {
    return (
      <Container style={c_styles.container}>
        <Header androidStatusBarColor="#9CDEE6" style={c_styles.bgColor}>
          <Left>
            <Button
              transparent
              onPress={() => this.props.navigation.navigate('Legal')}>
              <Icon name="arrow-back" />
            </Button>
          </Left>
          <Body>
            <Title>{this.state.title}</Title>
          </Body>
          <Right>
            <Button transparent onPress={() => this.props.navigation.navigate('Notifications')}>
              <Icon style={{ fontSize: 35 }} name="notifications" />
              <Badge style={{ position: 'absolute',marginLeft: 30 }}><Text>2</Text></Badge>
            </Button>
          </Right>
        </Header>
        <Content padder>
          <ScrollView style={{ flex: 1 }}>
           <HTML html={this.state.content}/>
          </ScrollView>
        </Content>
      </Container>

    );
  }
}
