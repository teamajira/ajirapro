import React, {Component}  from 'react';
import { AsyncStorage,ImageBackground, Image,Keyboard } from "react-native";
import {Container,Header,Title,H1,View,Content,Button,Item,Label,Input,Body,Left,Right,Icon,Form,Text,Toast} from "native-base";
import c_styles from "../common_styles";
import styles from "./styles";

import FormData from 'FormData';
import DeviceInfo from 'react-native-device-info';

const banner = require("AjiraPro/assets/block.png");
const bannerLogo = require("AjiraPro/assets/key.png");

import { HASH,API_URL,APP_KEY,APP_VERSION,API_CHANGE_PASS }  from "AjiraPro/src/screens/common_scripts";

let sha1 = require('sha1');
const deviceId = DeviceInfo.getUniqueID();
const osVersion = DeviceInfo.getVersion();

export default class ChangePassword extends Component {

constructor(props){
  super(props);

  this.state = {
        error: "",
        oldPass: "",
        password: "",
        confirmPass: "",
        token: "",
        hiddenOld: true,
        hiddenNew: true,
        hiddenConfirm: true,
        iconOld: "md-eye-off",
        iconNew: "md-eye-off",
        iconConfirm: "md-eye-off",
    }
  }


  componentDidMount() {

     this._loadData();

  }

  async _loadData () {
    try{
      let response = await AsyncStorage.getItem('loginToken');
      let error = await AsyncStorage.getItem('errorCode');

      if(error === "Inactive"){
        error = "Documents";
      }else{
        error = "ProArea";
      }

      this.setState({
        token: response,
        error: error
      });

    }catch (error) {
      this.setState({isLoading: false});
      console.log('AsyncStorage error: ' + error.message);
    }
  }

    changePassword = () =>{

    rand = sha1(new Date().getTime());
    url = API_URL + API_CHANGE_PASS;

    const formData = new FormData();
    formData.append('hash', HASH(API_CHANGE_PASS,rand));
    formData.append('rand', rand);
    formData.append('uuid', deviceId);
    formData.append('token', this.state.token);
    formData.append('oldPassword', this.state.oldPass);
    formData.append('newPassword', this.state.password);
    formData.append('confirmPassword', this.state.confirmPass);

    let postData = {
      method: 'POST',
      headers: {
          'Accept': 'application/json',
          'Content-Type': 'multipart/form-data'
      },
      body:formData
    }

    //console.log("Change Pass postdata:"+ JSON.stringify(postData));

    fetch(url, postData)
       .then(response => {
         return response.json()
       })
       .then((responseJson) => {
         //console.log("Change Pass response:"+ JSON.stringify(responseJson));
         Keyboard.dismiss();
         let message = responseJson.settings.message

         if(responseJson.settings.success){
           message = "Password Successfully Changed"
           this.setState({
             oldPass: "",
             password: "",
             confirmPass: "",
             hiddenOld: true,
             hiddenNew: true,
             hiddenConfirm: true,
             iconOld: "md-eye-off",
             iconNew: "md-eye-off",
             iconConfirm: "md-eye-off"
           })
           this.forceUpdate();
         }

         Toast.show({
           text: message,
           textStyle: { color: "yellow" },
           duration: 5000,
           buttonText: "Dismiss"});

    })
    .catch((error) =>{
      if (error == 'TypeError: Network request failed'){
        Toast.show({
          text: "Kindly check your network connection and try again",
          textStyle: { color: "red" },
          buttonText: "Dismiss"});
      }
    })

   }

   hidePassword = (choice,hidden) => {

     this.setState({ [hidden]: !this.state[hidden] })

     if (this.state[hidden]) {
       this.setState({ [choice]: "md-eye"})
     } else {
       this.setState({ [choice]: "md-eye-off" })
     }
   }

   password_validate(choice,pass) {

     if (pass.length < 8) {
       return(choice+"Password is too short");
     } else if (pass.length > 32) {
       return(choice+"Password is too long");
     } else if (pass.search(/\d/) == -1) {
       return(choice+"Password requires at least one number");
     } else if (pass.search(/[a-zA-Z]/) == -1) {
       return(choice+"Password requires at least one letter");
     } else if (pass.search(/[\!\@\#\$\%\^\&\*\(\)\_\+]/) == -1) {
       return(choice+"Password requires a Special character");
     }

     return true
   }

   validation = () =>{
     let error = null;
     if (this.state.oldPass.trim() === "") {
       error = "Old Password required.";
     }else if (this.state.password.trim() === "") {
       error = "New Password required.";
     }else if (this.state.confirmPass.trim() === "") {
       error = "Confirm Password required.";
     }else if(this.state.password !== this.state.confirmPass){
       error = "Passwords don't match.";
     }else if (this.password_validate("",this.state.oldPass.trim()) !== true){
        error = this.password_validate("Old ",this.state.oldPass.trim())
     }else if (this.password_validate("",this.state.password.trim()) !== true){
        error = this.password_validate("New ",this.state.password.trim())
     }else if (this.password_validate("",this.state.confirmPass.trim()) !== true){
        error = this.password_validate("Confirm ",this.state.confirmPass.trim())
     }else{
        error = null;
     }

     if(error === null){
       this.changePassword();
     }else{
       Toast.show({
         text: error,
         duration: 3000,
         buttonTextStyle: {color: "yellow"},
         buttonText: "Dismiss"});
     }
   }

  render() {
    return (
      <Container style={c_styles.container}>
        <Header androidStatusBarColor="#9CDEE6" hasSegment style={c_styles.bgColor}>
          <Left>
            <Button transparent onPress={() => this.props.navigation.navigate(this.state.error)}>
              <Icon name="arrow-back" />
            </Button>
          </Left>
          <Body>
            <Title>Change Password</Title>
          </Body>
          <Right />
        </Header>

        <Content>
          <ImageBackground source={banner} style={styles.cover} >
            <View style={styles.logoContainer}>
              <Image source={bannerLogo} style={styles.logo} />
            </View>
          </ImageBackground>
          <H1 style={c_styles.alignCenter}> Change password ?</H1>
          <Text style={[c_styles.alignCenter,c_styles.colorGrey]}>Enter you new password and update</Text>

          <Form style={c_styles.formSpacing}>
            <Item floatingLabel>
              <Label>Old Password</Label>
              <Input
                style={{marginTop: 10}}
                maxLength={32}
                minLength={8}
                returnKeyType={ 'next' }
                value={ this.state.oldPass }
                secureTextEntry={this.state.hiddenOld}
                onChangeText={oldPass => this.setState({oldPass})}/>
                <Icon size={25}  name={this.state.iconOld} onPress={() => this.hidePassword("iconOld","hiddenOld")} style={{color:"#333"}} />
            </Item>

            <Item floatingLabel>
              <Label>New Password</Label>
              <Input
                style={{marginTop: 10}}
                maxLength={32}
                returnKeyType={ 'next' }
                value={ this.state.password }
                secureTextEntry={this.state.hiddenNew}
                onChangeText={password => this.setState({password})}/>
                <Icon size={25}  name={this.state.iconNew} onPress={() => this.hidePassword("iconNew","hiddenNew")} style={{color:"#333"}} />
            </Item>

            <Item floatingLabel>
              <Label>Confirm Password</Label>
              <Input
                style={{marginTop: 10}}
                maxLength={32}
                returnKeyType={ 'done' }
                value={ this.state.confirmPass }
                secureTextEntry={this.state.hiddenConfirm}
                onChangeText={confirmPass => this.setState({confirmPass})}/>
                <Icon size={25} name={this.state.iconConfirm} onPress={() => this.hidePassword("iconConfirm","hiddenConfirm")} style={{color:"#333"}} />
            </Item>
          </Form>

          <Button block rounded style={c_styles.buttonT25} onPress={this.validation}>
            <Text>Update Password</Text>
          </Button>

        </Content>
      </Container>
    );
  }
}
