import React, { Component } from "react";
import { NavigationActions } from 'react-navigation';
import { AsyncStorage,NetInfo, View, FlatList,ScrollView, RefreshControl } from "react-native";
import { Content,Text, Body, Left, Right, Icon, ListItem } from "native-base";
import FormData from 'FormData';
import DeviceInfo from 'react-native-device-info';

import c_styles from "../../common_styles";
import { COLOR_TURGUOISE,HASH,API_URL,APP_KEY,APP_VERSION, API_COMPLETED }  from "../../common_scripts";

let sha1 = require('sha1');

const deviceId = DeviceInfo.getUniqueID();
const osVersion = DeviceInfo.getVersion();

let completedList = null;
let completedQuote = null;
let completedDetails = null;

export default class Completed extends Component {

  constructor(props){
    super(props);

    this.state = {
        jsonData: "",
        token: "",
        isLoading:true
      }
   }

   componentDidMount() {

      this._loadData();

   }

   async _loadData () {
     try{
       let response = await AsyncStorage.getItem('loginToken');

       this.setState({token: response});

       this.getCompleted();
     }catch (error) {
       this.setState({isLoading: false});
       console.log('AsyncStorage error: ' + error.message);
     }
   }

   _onRefresh = () => {
     this.setState({isLoading: true});
     this.getCompleted();
   }

     getCompleted = () =>{

       let rand = sha1(new Date().getTime());
       let hash = HASH(API_COMPLETED,rand);
       let url = API_URL + API_COMPLETED;

       const formData = new FormData();
       formData.append('hash', hash);
       formData.append('rand', rand);
       formData.append('uuid', deviceId);
       formData.append('token', this.state.token);

       let postData = {
         method: 'POST',
         headers: {
             'Accept': 'application/json',
             'Content-Type': 'multipart/form-data'
         },
         body:formData
       }

       //console.log("Completed postdata:"+ JSON.stringify(postData));

       fetch(url, postData)
          .then(response => {
            return response.json()
          })
          .then((responseJson) => {
            //console.log("completed response:"+ JSON.stringify(responseJson));
            this.setState({isLoading: false});

            if(responseJson.settings.success){
              if(responseJson.data.completed.list){
                completedList = responseJson.data.completed.list;
                completedQuote = responseJson.data.completed.quote;
                completedDetails = responseJson.data.completed.detail;
              }

               this.setState({
                   //jsonData: JSON.stringify(responseJson.data, null, 2)
               });
            }
       })
       .catch((error) =>{
           if(error == 'TypeError: Network request failed'){
               alert('Kindly check if the device is connected to stable cellular data connection or WiFi.',);
               this.setState({isLoading: false});
           }
       })
     }

  _toUpper(str) {

   let value =
       str .toLowerCase()
           .split(' ')
           .map(function(word) {
               return word[0].toUpperCase() + word.substr(1);
           })
           .join(' ');

     if(value == "Completed"){
       return <View>
                 <Icon name="md-checkmark-circle" style={c_styles.iconList} />
                 <Text style={c_styles.textStatus}>{value}</Text>
               </View>
     }else if(value == "Cancelled"){
       return <View>
                 <Icon name="md-stopwatch" style={c_styles.iconList} />
                 <Text style={c_styles.textStatus}>{value}</Text>
              </View>
     }else{
       return <Text style={c_styles.textStatus}>{value} </Text>
     }

 }

 _completedDetails(serviceId){
   return <Text style={c_styles.textName}>{completedDetails[serviceId].requestType}</Text>
 }

 _completedPrice(serviceId){
   return <Text style={c_styles.textName}>{completedQuote[serviceId].cost}</Text>
 }

 _viewQuote(rootNav,request){

 try{
       const navigateAction = NavigationActions.navigate({
           routeName: 'ProAreaSingle',
           params: {
             requestData: request,
             quoteData: completedQuote[request.serviceId],
             detailData: completedDetails[request.serviceId],
           }
       })

       rootNav.dispatch(navigateAction);

     }catch (error) {
     // Error retrieving data
     console.log(error.message);
   }
 }

 _displayData(completedList,rootNav){

 if(completedList === null && !this.state.isLoading){
       return <Text style={c_styles.alignCenter}>You currently have no completed service requests.</Text>
  }else{
     return <FlatList
           data={completedList}
           keyExtractor={item => item.serviceId }
           renderItem={({item}) =>
           <ListItem avatar onPress={() => this._viewQuote(rootNav,item)}>
             <Left/>
             <Body>
               <View>
                 <Text>{this._toUpper(item.customerName)}</Text>
                 {this._completedDetails(item.serviceId)}
                 <Text note>{item.location}</Text>
                 <Text style={{fontSize: 10}} note style={c_styles.textOrange}>Total Price <Text>{this._completedPrice(item.serviceId)}</Text> </Text>
               </View>
             </Body>
             <Right>
                 <View style={c_styles.listBody}>
                   <View style={c_styles.seperator} />
                   {this._toUpper(item.status)}
                 </View>
             </Right>
           </ListItem>}
         />
     }
 }

  render() {
    let rootNav = this.props.screenProps.rootNavigation;

    return (
      <ScrollView
        refreshControl={
          <RefreshControl
            refreshing={this.state.isLoading}
            onRefresh={this._onRefresh}/>
          }>
          <Content padder>
            <Text>{this.state.jsonDataHired}</Text>
              {this._displayData(completedList,rootNav)}
          </Content>
      </ScrollView>
    );
  }
}
