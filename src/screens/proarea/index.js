import React, { Component } from "react";
import {
  Container,
  Header,
  Title,
  Button,
  Icon,
  Tabs,
  Tab,
  TabHeading,
  Right,
  Left,
  Body,
  ScrollableTab,
  Badge,
  Text
} from "native-base";

import Requests from "./requests";
import Hired from "./hired";
import Completed from "./completed";

import c_styles from "../common_styles";


class ProArea extends Component {
  render() {

    return (
      <Container >
        <Header hasTabs androidStatusBarColor="#9CDEE6" style={c_styles.bgColor}>
          <Left>
          <Button
            transparent
            onPress={() => this.props.navigation.navigate("DrawerOpen")}>
            <Icon name="menu" />
          </Button>
          </Left>
          <Body>
            <Title> Pro Area</Title>
          </Body>
          <Right>
            <Button transparent onPress={() => this.props.navigation.navigate('Notifications')}>
              <Icon style={{ fontSize: 35 }} name="notifications" />
              <Badge style={{ position: 'absolute',marginLeft: 30 }}><Text>2</Text></Badge>
            </Button>
          </Right>
        </Header>

        <Tabs renderTabBar={() => <ScrollableTab style={c_styles.bgColor} />}  >

          <Tab heading={
            <TabHeading style={c_styles.bgColor}>
                <Text style={c_styles.text}>Requests</Text>
            </TabHeading>}>
            <Requests
                // Pass these navigation props to child navigation
                // So that it's possible to navigate back to home
                screenProps={{ rootNavigation: this.props.navigation }} />
          </Tab>

          <Tab heading={
            <TabHeading style={c_styles.bgColor}>
                <Text style={c_styles.text}>Hired</Text>
            </TabHeading>}>
            <Hired screenProps={{ rootNavigation: this.props.navigation }} />
          </Tab>

          <Tab heading={
            <TabHeading style={c_styles.bgColor}>
                <Text style={c_styles.text}>Completed</Text>
            </TabHeading>}>
            <Completed screenProps={{ rootNavigation: this.props.navigation }} />
          </Tab>

        </Tabs>
      </Container>
    );
  }
}

export default ProArea;
