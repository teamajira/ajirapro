import React, { Component } from "react";
import moment from 'moment';
import { NavigationActions } from 'react-navigation';
import { ActivityIndicator,AsyncStorage,NetInfo, View,FlatList ,ScrollView, RefreshControl} from "react-native";
import { Content,Text, Body, Left, Right, Icon, ListItem } from "native-base";
import FormData from 'FormData';
import CryptoJS from 'crypto-js';
import DeviceInfo from 'react-native-device-info';

import c_styles from "AjiraPro/src/screens/common_styles";
import { COLOR_TURGUOISE,HASH,API_URL,APP_KEY,APP_VERSION, API_REQUESTS }  from "AjiraPro/src/screens/common_scripts";

let sha1 = require('sha1');

const deviceId = DeviceInfo.getUniqueID();
const osVersion = DeviceInfo.getVersion();

let requestList = null;
let requestQuote = null;
let requestDetails = null;

export default class Requests extends Component {

  constructor(props){
    super(props);

    this.state = {
        jsonData: "",
        token: "",
        isLoading:true
      }
   }

   componentDidMount() {

      this._loadData();

   }

   async _loadData () {
     try{
           let response = await AsyncStorage.getItem('loginData');
           let jsonDatas = await JSON.parse(response) || "";

           this.setState({
             token: jsonDatas.token,
           });

           this.getRequests();

         }catch (error) {
           this.setState({isLoading: false});
          console.log('AsyncStorage error: ' + error.message);
        }
     }

     _onRefresh = () => {
        this.setState({isLoading: true});
        this.getRequests();
      }

     getRequests = () =>{

       let rand = sha1(new Date().getTime());
       let hash = HASH(API_REQUESTS,rand);
       let url = API_URL + API_REQUESTS;

       const formData = new FormData();
       formData.append('hash', hash);
       formData.append('rand', rand);
       formData.append('uuid', deviceId);
       formData.append('token', this.state.token);

       let postData = {
         method: 'POST',
         headers: {
             'Accept': 'application/json',
             'Content-Type': 'multipart/form-data'
         },
         body:formData
       }

       //console.log("requests postdata:"+ JSON.stringify(postData));

       fetch(url, postData)
          .then(response => {
            return response.json()
          })
          .then((responseJson) => {
            //console.log("history response:"+ JSON.stringify(responseJson));

            this.setState({isLoading: false});

            if(responseJson.settings.success){
              if(responseJson.data.requested.list){
                requestList = responseJson.data.requested.list;
                requestQuote = responseJson.data.requested.quote;
                requestDetails = responseJson.data.requested.detail;
              }
               this.setState({
                   //jsonData: JSON.stringify(responseJson.data.requested.detail, null, 2)
               });
            }
       })
       .catch((error) =>{
           if(error == 'TypeError: Network request failed'){
               alert('Kindly check if the device is connected to stable cellular data connection or WiFi.',);
               this.setState({isLoading: false});
           }
       })
     }


      _requestDetails(serviceId){
        return <Text style={c_styles.textName}>{requestQuote[serviceId].requestType}</Text>
      }

    _toUpper(str) {

       let value =
           str .toLowerCase()
               .split(' ')
               .map(function(word) {
                   return word[0].toUpperCase() + word.substr(1);
               })
               .join(' ');

         if(value == "Completed"){
           return <View>
                     <Icon name="md-checkmark-circle" style={c_styles.iconList} />
                     <Text style={c_styles.textStatus}>{value}</Text>
                   </View>
         }else if(value == "Pending"){
           return <View>
                     <Icon name="md-stopwatch" style={c_styles.iconList} />
                     <Text style={c_styles.textStatus}>{value}</Text>
                  </View>
         }else{
           return value
         }

     }

_viewQuote(rootNav,request){

try{
      const navigateAction = NavigationActions.navigate({
          routeName: 'ProAreaSingle',
          params: {
            requestData: request,
            quoteData: requestQuote[request.serviceId],
            detailData: requestDetails[request.serviceId]
          }
      })

      rootNav.dispatch(navigateAction);

    }catch (error) {
    // Error retrieving data
    console.log(error.message);
  }
}

 _displayData(requestList,rootNav){
   if(requestList === null && !this.state.isLoading){
       return <Text style={c_styles.alignCenter}>You currently have no requests.</Text>
   }else{
     return <FlatList
         data={requestList}
         keyExtractor={item => item.serviceId }
         renderItem={({item}) =>
           <ListItem avatar onPress={() => this._viewQuote(rootNav,item)}>
             <Left/>
             <Body>
               <View>
                 <Text style={c_styles.textStatus}>{this._toUpper(item.customerName)}</Text>
                 {this._requestDetails(item.serviceId)}
                 <Text note>{item.location}</Text>
                 <Text style={{fontSize: 10}} note>{moment(item.postedDate).format('Do MMM YYYY hh:mm a')}</Text>
               </View>
             </Body>
             <Right>
                 <View style={c_styles.listBody}>
                   <View style={c_styles.seperator} />
                   {this._toUpper(item.status)}
                 </View>
             </Right>
           </ListItem>}
         />
     }
 }

  render() {

    let rootNav = this.props.screenProps.rootNavigation;

    return (
      <ScrollView
        refreshControl={
          <RefreshControl
            refreshing={this.state.isLoading}
            onRefresh={this._onRefresh}/>
          }>
          <Content padder>
            <Text>{this.state.jsonDataHired}</Text>
              {this._displayData(requestList,rootNav)}
          </Content>
      </ScrollView>
    );
  }
}
