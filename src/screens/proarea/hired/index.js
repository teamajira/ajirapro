import React, { Component } from "react";
import { NavigationActions } from 'react-navigation';
import Icon from 'react-native-vector-icons/AntDesign';
import { ActivityIndicator, AsyncStorage, NetInfo, View, FlatList, ScrollView, RefreshControl  } from "react-native";
import { Content,Text, Body, Left, Right, List, ListItem} from "native-base";
import FormData from 'FormData';
import CryptoJS from 'crypto-js';
import DeviceInfo from 'react-native-device-info';

import c_styles from "../../common_styles";
import { COLOR_TURGUOISE,HASH,API_URL,APP_KEY,APP_VERSION, API_HIRED }  from "../../common_scripts";

let sha1 = require('sha1');

const deviceId = DeviceInfo.getUniqueID();
const osVersion = DeviceInfo.getVersion();

let hiredList = null;
let hiredQuote = null;
let hiredDetails = null;

export default class Hired extends Component {

  constructor(props){
    super(props);

    this.state = {
        jsonDataHired: "",
        token: "",
        isLoading:true
      }
   }

   componentDidMount() {

      this._loadData();

   }

   async _loadData () {
     try{
           let response = await AsyncStorage.getItem('loginData');
           let jsonDatas = await JSON.parse(response) || "";

           this.setState({
             token: jsonDatas.token
           });

           this.getHired();

         }catch (error) {
           this.setState({isLoading: false});
          console.log('AsyncStorage error: ' + error.message);
        }
     }

     _onRefresh = () => {
        this.setState({isLoading: true});
        this.getHired();
      }

     getHired = () =>{

       let rand = sha1(new Date().getTime());
       let hash = HASH(API_HIRED,rand);
       let url = API_URL + API_HIRED;

       const formData = new FormData();
       formData.append('hash', hash);
       formData.append('rand', rand);
       formData.append('uuid', deviceId);
       formData.append('token', this.state.token);

       let postData = {
         method: 'POST',
         headers: {
             'Accept': 'application/json',
             'Content-Type': 'multipart/form-data'
         },
         body:formData
       }

       //console.log("hired postdata:"+ JSON.stringify(postData));

       fetch(url, postData)
          .then(response => {
            return response.json()
          })
          .then((responseJson) => {
            //console.log("hired response:"+ JSON.stringify(responseJson));
            this.setState({isLoading: false});

            if(responseJson.settings.success){
              if(responseJson.data.hired.list){
                hiredList = responseJson.data.hired.list;
                hiredQuote = responseJson.data.hired.quote;
                hiredDetails = responseJson.data.hired.detail;
              }

               this.setState({
                   //jsonDataHired: JSON.stringify(responseJson.data.hired, null, 2)
               });
            }
       })
       .catch((error) =>{
           if(error == 'TypeError: Network request failed'){
               alert('Kindly check if the device is connected to stable cellular data connection or WiFi.',);
               this.setState({isLoading: false});
           }
       })

     }


  _toUpper(str) {

   let value =
       str .toLowerCase()
           .split(' ')
           .map(function(word) {
               return word[0].toUpperCase() + word.substr(1);
           })
           .join(' ');

     if(value === "Accepted" || value === "Completed"){
       return <View>
                 <Icon name="checkcircle" style={c_styles.iconList} />
                 <Text style={c_styles.textStatus}>{value}</Text>
               </View>
     }else if(value === "Declined"){
       return <View>
                 <Icon name="closecircle" style={c_styles.iconList} />
                 <Text style={c_styles.textStatus}>{value}</Text>
              </View>
     }else if (value === "Progress"){
       return <View>
                 <Icon name="clockcircleo" style={c_styles.iconList} />
                 <Text style={c_styles.textStatus}>{value}</Text>
              </View>

     }else{
       return <Text style={c_styles.textStatus} >{value} </Text>
     }

 }

 _requestDetails(serviceId){
   return <Text style={c_styles.textName}>{hiredDetails[serviceId].requestType}</Text>
 }

 _requestPrice(serviceId){
   return <Text style={c_styles.textName}>{hiredQuote[serviceId].cost}</Text>
 }

 _viewQuote(rootNav,request){

 try{
       const navigateAction = NavigationActions.navigate({
           routeName: 'ProAreaSingle',
           params: {
             requestData: request,
             quoteData: hiredQuote[request.serviceId],
             detailData: hiredDetails[request.serviceId],
           }
       })

       rootNav.dispatch(navigateAction);

     }catch (error) {
     // Error retrieving data
     console.log(error.message);
   }
 }

 _displayData(hiredList,rootNav){
   if(hiredList === null && !this.state.isLoading){
       return <Text style={c_styles.alignCenter}>You currently have no hired service requests.</Text>
   }else{
     return <FlatList
           data={hiredList}
           keyExtractor={item => item.serviceId }
           renderItem={({item}) =>
           <ListItem avatar onPress={() => this._viewQuote(rootNav,item)}>
             <Left/>
             <Body>
               <View>
                 <Text>{this._toUpper(item.customerName)}</Text>
                 {this._requestDetails(item.serviceId)}
                 <Text note>{item.location}</Text>
                 <Text style={{fontSize: 10}} note style={c_styles.textOrange}>Total Price <Text>{this._requestPrice(item.serviceId)}</Text> </Text>
               </View>
             </Body>
             <Right>
                 <View style={c_styles.listBody}>
                   <View style={c_styles.seperator} />
                   {this._toUpper(item.status)}
                 </View>
             </Right>
           </ListItem>}
         />
     }
 }

  render() {
    let rootNav = this.props.screenProps.rootNavigation;

    return (
      <ScrollView
        refreshControl={
          <RefreshControl
            refreshing={this.state.isLoading}
            onRefresh={this._onRefresh}/>
          }>
          <Content padder>
            <Text>{this.state.jsonDataHired}</Text>
              {this._displayData(hiredList,rootNav)}
          </Content>
      </ScrollView>
    );
  }
}
