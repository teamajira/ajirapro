import React, { Component } from "react";
import {ImageBackground,Image,View,Keyboard,AsyncStorage,NetInfo} from "react-native";
import {NavigationActions} from 'react-navigation';
import {Container,H1,Header,Title,Content,Button,Item,Label,Input,Body,Left,Right,Icon,Form,Text,Toast} from "native-base";
import FormData from 'FormData';
import DeviceInfo from 'react-native-device-info';
import DialogProgress from 'react-native-dialog-progress'; {
  /*import RNFirebase from 'react-native-firebase';*/ }

import styles from "./styles";
import common_styles from "../common_styles";
import { HASH,API_URL,APP_KEY,APP_VERSION,API_LOGIN} from "../common_scripts";

const loginCover = require("AjiraPro/assets/app-cover.png");
const launchscreenLogo = require("AjiraPro/assets/logopro.png");

let sha1 = require('sha1');

const options = {
  title: "Signing In",
  message: "Please wait...",
  isCancelable: true
}

const deviceId = DeviceInfo.getUniqueID();
const osVersion = DeviceInfo.getVersion();

export default class Login extends Component {

  constructor(props) {
    super(props);

    this.state = {
      email: "0720111222",
      password: "123456@a",
      isLoading: true,
      loginData: "",
      address: "Not Available",
      firebaseToken: "",
      backgroundColor: "transparent",
      hidden: true,
      jsonData: "",
      iconName: "md-eye-off"
    }

  }

  onFocus = () => {
    this.setState({
      backgroundColor: "#fff7eb"
    })
  }

  onBlur = () => {
    this.setState({
      backgroundColor: "transparent"
    })
  }

  hidePassword = () => {
    this.setState({hidden: !this.state.hidden})
    if (this.state.hidden) {
      this.setState({iconName: "md-eye"})
    } else {
      this.setState({iconName: "md-eye-off"})
    }
  }

  componentDidMount() {
    {
      /*
              firebase.messaging().getToken()
               .then((token) => {
                  console.log('Device FCM Token: ', token);
                  this.setState({"firebaseToken": token});
              });
              */
    }

  }


  UserLogin = () => {

    DialogProgress.show(options);

    rand = sha1(new Date().getTime());
    url = API_URL + API_LOGIN;

    const { email } = this.state;
    const { password } = this.state;
    const navigateAction = NavigationActions.navigate({
      routeName: 'ProArea'
    })

    const navigateActionDoc = NavigationActions.navigate({
      routeName: 'Documents'
    })

    const formData = new FormData();
    formData.append('hash', HASH(API_LOGIN, rand));
    formData.append('rand', rand);
    formData.append('uuid', deviceId);
    formData.append('osVersion', osVersion);
    formData.append('email', email);
    formData.append('password', password);
    formData.append('firebase_id', "testtesttesttestestestest"); {
      /*this.state.firebaseToken*/ }
    let postData = {
      method: 'POST',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'multipart/form-data'
      },
      body: formData
    }

    console.log("login postData:" + JSON.stringify(postData));

    NetInfo.isConnected.fetch().then((isConnected) => {
      if (isConnected) {
        fetch(url, postData)
          .then(response => {
            return response.json()
          })
          .then((responseJson) => {

            console.log("login response:" + JSON.stringify(responseJson));
            Keyboard.dismiss();
            if (responseJson.settings.success) {
              if (responseJson.settings.errorCode === "L002") {
                AsyncStorage.setItem("errorCode", responseJson.settings.errorCode);
                let navigateVerify = NavigationActions.navigate({
                  routeName: 'OTP',
                  params: {
                    otpSalt: responseJson.data.otpSalt,
                    uid:responseJson.data.uid
                   }
                });
                this.props.navigation.dispatch(navigateVerify);
              } else {
                AsyncStorage.setItem("loginData", JSON.stringify(responseJson.data));
                AsyncStorage.setItem("firstName", responseJson.data.firstName);
                AsyncStorage.setItem("personalPhoto", responseJson.data.personalPhoto);
                AsyncStorage.setItem("loginToken", responseJson.data.token);
                AsyncStorage.setItem("errorCode", responseJson.data.status);
                if(responseJson.data.status === "Inactive"){
                  this.props.navigation.dispatch(navigateActionDoc);
                }else{
                  this.props.navigation.dispatch(navigateAction);
                }

              }
            }
            Toast.show({
              text: responseJson.settings.message,
              duration: 3000,
              buttonTextStyle: { color: "yellow" },
              buttonText: "Dismiss"});

          })
          .catch((error) => {
            console.log(JSON.stringify(error));
            if (error == 'TypeError: Network request failed') {
              alert('Unable to login, contact administrator', );
            }
          })
      } else {
        alert("Kindly check if the device is connected to stable cellular data connection or WiFi.");
      }
    });

    DialogProgress.hide()
  }

  render() {
    return (
      <Container style={common_styles.container}>
      <ImageBackground source={loginCover} style={styles.cover} >
          <View style={styles.logoContainer}>
            <Image source={launchscreenLogo} style={common_styles.logo} />
          </View>
          <Header androidStatusBarColor="#9CDEE6" style={{ display: "none" }} />
      </ImageBackground>
        <Content>
          <View
            style={{
              alignItems: "center",
              marginTop: 15,
              backgroundColor: "transparent"
            }}>
            <View style={{ marginTop: 10 }} />
            <H1 style={styles.text}>Welcome back</H1>
          </View>
          <Form style={common_styles.formSpacing}>
            <Item floatingLabel>
              <Label style={common_styles.colorGrey}>Phone Number</Label>
              <Input style={{marginTop: 10}}
                  keyboardType = "numeric"
                  returnKeyType={ 'next' }
                  onBlur={ () => this.onBlur() }
                  onFocus={ () => this.onFocus() }
                  value={ this.state.email }
                  onChangeText={(email) => this.setState({email})}/>
            </Item>
            <Item floatingLabel>
              <Label>Password</Label>
              <Input
                style={{marginTop: 10}}
                returnKeyType={ 'done' }
                onBlur={ () => this.onBlur() }
                onFocus={ () => this.onFocus() }
                value={ this.state.password }
                secureTextEntry={this.state.hidden}
                onChangeText={password => this.setState({password})}/>
                <Icon name={this.state.iconName} onPress={() => this.hidePassword()} style={{color:"#333"}} />
            </Item>
          </Form>
          <Button block rounded
                  style={common_styles.buttonT25}
                  onPress={this.UserLogin}>
            <Text>Sign In</Text>
          </Button>

          <View style={{ margin: 10 }} >
            <Text style={{textAlign: "center"}}>Already have an account ?
              <Text>{this.state.jsonData}</Text>
              <Text
              style={[common_styles.underline]}
              onPress={() => this.props.navigation.navigate('Register')}> Register
               </Text>
            </Text>
          </View>

          <Text style={[common_styles.alignCenter,common_styles.colorGrey,common_styles.underline]}
                onPress={() => this.props.navigation.navigate('ForgotPassword')}>
                 Forgot your password ?
          </Text>
        </Content>
      </Container>
    );
  }
}
