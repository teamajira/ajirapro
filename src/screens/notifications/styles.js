import { COLOR_TURGUOISE } from "../common_scripts";

export default {
  container: {
    backgroundColor: "#FFF"
  },
  text: {
    alignSelf: "center",
    marginBottom: 7
  },
  textTitle: {
    color: COLOR_TURGUOISE,
    fontSize: 18
  },
  mb: {
    marginBottom: 15
  }
};
