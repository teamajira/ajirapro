import React, { Component } from "react";
import moment from 'moment';
import { FlatList,View } from "react-native";
import { Container,Header,Title,Content,Button,Badge,Icon,Left,Right,Body,Text,ListItem,Toast} from "native-base";

import styles from "../styles";
import c_styles from "../../common_styles";


export default class SingleNotification extends Component {
  constructor(props){
    super(props);

    this.state = {
      notification: ""
    }
  }

  componentDidMount() {
    this._isMounted = true;

    if (this._isMounted) {
      var { params } = this.props.navigation.state;
      notificationData = params ? params.notification : null
      console.log(JSON.stringify(notificationData));

      this.setState({
        notification:notificationData
      })
    }
  }

  componentWillUnmount() {
    this._isMounted = false;
  }

  render() {
    return (

      <Container style={c_styles.container}>
        <Header androidStatusBarColor="#9CDEE6" style={c_styles.bgColor}>
          <Left>
            <Button
              transparent
              onPress={() => this.props.navigation.navigate("Notifications")}>
              <Icon name="arrow-back" />
            </Button>
          </Left>
          <Body>
            <Title>{this.state.notification.title}</Title>
          </Body>
          <Right/>
        </Header>

        <Content padder>
          <Text>{/*JSON.stringify(this.state.notification)*/}</Text>
          <ListItem avatar>
            <Left/>
            <Body>
              <Text style={styles.textTitle}>{this.state.notification.title}</Text>
              <Text>{this.state.notification.message}</Text>
              <Text note> {moment(this.state.notification.postedDate).format('Do MMM YYYY hh:mm a')} </Text>
            </Body>
          </ListItem>
        </Content>
      </Container>
    );
  }
}
