import React, { Component } from "react";
import moment from 'moment';
import { NavigationActions } from 'react-navigation';
import { AsyncStorage,NetInfo,FlatList,View,ScrollView, RefreshControl } from "react-native";
import { Container,Header,Title,Content,Button,Badge,Icon,Left,Right,Body,Text,ListItem,Toast} from "native-base";
import FormData from 'FormData';
import DeviceInfo from 'react-native-device-info';

import styles from "./styles";
import c_styles from "../common_styles";

import { HASH,API_URL,APP_KEY,APP_VERSION, API_NOTIFICATIONS,API_NOTIFICATIONS_READ }  from "../common_scripts";

let sha1 = require('sha1');
const deviceId = DeviceInfo.getUniqueID();
const osVersion = DeviceInfo.getVersion();

let notificationData = null;

const isUnRead = {
  backgroundColor:"#f4f4ff",
  marginLeft:-5
}

const isRead = {
  backgroundColor:"#ffffff",
  marginLeft:-5
}

export default class Notifications extends Component {
  constructor(props){
    super(props);

    this.state = {
      jsonData: "",
      token: "",
      isLoading: true
    }
  }

  componentDidMount() {
    this._isMounted = true;
    this._loadData();

  }

  componentWillUnmount() {
    this._isMounted = false;
  }

  async _loadData () {
    try{
      let response = await AsyncStorage.getItem('loginToken');

      this.setState({token: response});
      this.getNotifications(0);

    }catch (error) {
      console.log('AsyncStorage error: ' + error.message);
    }
  }

  getNotifications = (id) =>{

    let rand = sha1(new Date().getTime());
    let hash = null;
    let url = null;
    if(id === 0){
      hash = HASH(API_NOTIFICATIONS,rand);
      url = API_URL + API_NOTIFICATIONS;
    }else{
      hash = HASH(API_NOTIFICATIONS_READ,rand);
      url = API_URL + API_NOTIFICATIONS_READ;
    }


    const formData = new FormData();
    formData.append('hash', hash);
    formData.append('rand', rand);
    formData.append('uuid', deviceId);
    formData.append('token', this.state.token);

    if(id !== 0){
      formData.append('notificationId', id);
    }

    let postData = {
      method: 'POST',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'multipart/form-data'
      },
      body:formData
    }

    //console.log("notifications postdata:"+ JSON.stringify(postData));

    fetch(url, postData)
    .then(response => {
      return response.json()
    })
    .then((responseJson) => {
      if (this._isMounted) {
        console.log("notification response:"+ JSON.stringify(responseJson));

        this.setState({isLoading: false});

        if(responseJson.settings.success) {
          if(responseJson.data.notifications){
            notificationData =  responseJson.data.notifications;
          }

          this.setState({
            //jsonData: JSON.stringify(responseJson.data.notifications, null, 2)
          });
        }
      }

    })
    .catch((error) =>{
      Toast.show({
        text: "Unable to load notifications",
        textStyle: { color: "red" },
        buttonText: "Dismiss"});

        this.setState({isLoading: false});
      })
    }

    _singleNotification(item){
      this.getNotifications(item.id);

      try{
            const navigateAction = NavigationActions.navigate({
                routeName: 'SingleNotification',
                params: {
                  notification: item
                }
            })

            this.props.navigation.dispatch(navigateAction);

          }catch (error) {
          // Error retrieving data
          console.log(error.message);
        }
    }

    _isUnread(read){
      console.log("Read Status "+read);
      if(read === "1"){
        return isRead
      }else{
        return isUnRead
      }
    }

    _displayData(notificationData){
      if(notificationData === null && !this.state.isLoading){
        return <Text style={c_styles.alignCenter}> You currently have no notifications.</Text>
      }else{
        return <FlatList
        data={notificationData}
        keyExtractor={item => item.id}
        renderItem={({item}) =>
        <ListItem avatar style={this._isUnread(item.read)} onPress={() => this._singleNotification(item)}>
          <Left/>
          <Body>
            <Text> {item.title}</Text>
            <Text numberOfLines={1} note> {item.message}</Text>
          </Body>
          <Right>
            <Text note> {moment(item.postedDate).format('Do MMM YYYY hh:mm a')} </Text>
          </Right>
        </ListItem>
      }/>
    }
  }

  render() {
    return (

      <Container style={c_styles.container}>
        <Header androidStatusBarColor="#9CDEE6" style={c_styles.bgColor}>
          <Left>
            <Button
              transparent
              onPress={() => this.props.navigation.navigate("DrawerOpen")}>
              <Icon name="menu" />
            </Button>
          </Left>
          <Body>
            <Title>Notifications</Title>
          </Body>
          <Right>
            <Button transparent onPress={() => this.props.navigation.navigate('Notifications')}>
              <Icon style={{ fontSize: 35 }} name="notifications" />
              <Badge style={{ position: 'absolute',marginLeft: 30 }}><Text>2</Text></Badge>
            </Button>
          </Right>
        </Header>



        <ScrollView
          refreshControl={
            <RefreshControl
              refreshing={this.state.isLoading}
              onRefresh={this._onRefresh}/>
            }>

          <Content>
          <Text>
          {this.state.jsonData}
          </Text>
          {this._displayData(notificationData)}
          </Content>
        </ScrollView>
      </Container>
    );
  }
}
