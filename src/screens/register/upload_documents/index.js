import React, { Component } from "react";
import Dialog from "react-native-dialog";
import FontAwesomeIcon from 'react-native-vector-icons/FontAwesome';
import { ActivityIndicator,AsyncStorage,NetInfo,View,Image,Linking,Platform } from "react-native";
import {Container,Header,Title,Content,Segment,Button,Item,Label,Input,Body,Left,Right,Form,Icon,Text,Badge,ListItem} from "native-base";
import c_styles from "AjiraPro/src/screens/common_styles";

import { Grid, Col } from "react-native-easy-grid";
import { DocumentPicker, DocumentPickerUtil } from 'react-native-document-picker';
const fileImage = require("AjiraPro/assets/pdf.png");

const options = {
    title: 'File Picker',
    chooseFileButtonTitle: 'Choose File...'
};

class Register extends Component {
  constructor(props) {
    super(props);
    this.state = {
      seg: 1,
      mainCategory: "Select Category",
      professions: "Select Category Type",
      services: "Select Service",
      location: "",
      nationalID:"",
      certificateNumber:"",
      fileID: "",
      fileGoodConduct: "",
      section: "",
      dialogVisible: false
    };
  }

  showDialog = () => {
    this.setState({ dialogVisible: true });
  };

  handleCancel = () => {
    this.setState({ dialogVisible: false });
  };

  handleOK = () => {
    this.setState({ dialogVisible: false });
  };

  onValueChange(value: string) {
    this.setState({
      selected: value
    });
  }

  loadFile(file_type){
    return DocumentPicker.show({
      filetype: [DocumentPickerUtil.allFiles()],
      //All type of Files DocumentPickerUtil.allFiles()
      //Only PDF DocumentPickerUtil.pdf()
      //Audio DocumentPickerUtil.audio()
      //Plain Text DocumentPickerUtil.plainText()
    },
    (error, res) => {
      console.log("Error "+JSON.stringify(error) );
      console.log('res : ' + JSON.stringify(res));
      console.log('URI : ' + res.uri);
      console.log('Type : ' + res.type);
      console.log('File Name : ' + res.fileName);
      console.log('File Size : ' + res.fileSize);

      if(file_type === "national_id"){
        this.setState({fileID: res});
      }else if(file_type === "good_conduct"){
        this.setState({fileGoodConduct: res});
      }

    }
  );
}

  render() {
    return (
      <Container style={c_styles.container}>
      <Header hasTabs androidStatusBarColor="#9CDEE6" style={c_styles.bgColor}>
        <Left>
        <Button
          transparent
          onPress={() => this.props.navigation.navigate("DrawerOpen")}>
          <Icon name="menu" />
        </Button>
        </Left>
        <Body>
          <Title>Professional Details</Title>
        </Body>
        <Right>
          <Button transparent onPress={() => this.props.navigation.navigate('Notifications')}>
            <Icon style={{ fontSize: 35 }} name="notifications" />
            <Badge style={{ position: 'absolute',marginLeft: 30 }}><Text>2</Text></Badge>
          </Button>
        </Right>
      </Header>

        <Segment style={c_styles.bgColor}>
          <Button
            first
            style={c_styles.roundLeft}
            active={this.state.seg === 1 ? true : false}
            onPress={() => this.setState({ seg: 1 })}>
            <Text>Select Service</Text>
          </Button>
          <Button
            style={c_styles.roundRight}
            active={this.state.seg === 2 ? true : false}
            onPress={() => this.setState({ seg: 2 })}>
            <Text>Upload Docs</Text>
          </Button>
        </Segment>

        <View>
          <Dialog.Container visible={this.state.dialogVisible}>
            <Dialog.Title style={c_styles.alignCenter}>Title</Dialog.Title>
            <Dialog.Description>
              Do you want to delete this account? You cannot undo this action.
            </Dialog.Description>
            <Dialog.Button label="Cancel" onPress={this.handleCancel}  />
            <Dialog.Button label="OK" onPress={this.handleOK} />
          </Dialog.Container>
        </View>

        <Content>
          <Form>
          {/* Individual */}
          {
              this.state.seg === 1 &&
            <View style={{margin:25}}>
              <Label style={{marginTop: 5}}>Select Main Category</Label>
              <ListItem
                button
                onPress={this.showDialog}>
                <Body>
                  <Text>
                    {this.state.mainCategory}
                  </Text>
                </Body>
                <Right>
                  <FontAwesomeIcon size={20} name="caret-down" style={{ color: "#999" }} />
                </Right>
              </ListItem>
              {/*
              <Button rounded style={c_styles.buttonT25} onPress={() => this.loadFile()}>
                <Text>Get File</Text>
              </Button>
              */}
              <Label style={{marginTop: 5}}>Select Professions</Label>
              <ListItem
                button
                onPress={this.showDialog}>
                <Body>
                  <Text>
                    {this.state.professions}
                  </Text>
                </Body>
                <Right>
                  <FontAwesomeIcon size={20} name="caret-down" style={{ color: "#999" }} />
                </Right>
              </ListItem>

              <Label style={{marginTop: 5}}>Select Services</Label>
              <ListItem
                button
                onPress={() => this.showDialog("category")}>
                <Body>
                  <Text>
                    {this.state.services}
                  </Text>
                </Body>
                <Right>
                  <FontAwesomeIcon size={20} name="caret-down" style={{ color: "#999" }} />
                </Right>
              </ListItem>

              <Item floatingLabel>
                <Label>Location</Label>
                <Input style={{marginTop: 5}} returnKeyType={ 'done' } />
              </Item>
            </View>
          }

          {/*Business*/}
          {
            this.state.seg === 2 &&
            <View style={c_styles.formSpacing}>
              <Item floatingLabel>
                <Label>National ID Number</Label>
                <Input style={{marginTop: 10}} returnKeyType={ 'next' } />
              </Item>
              <Item floatingLabel>
                <Label>Certificate of Good Conduct Number</Label>
                <Input style={{marginTop: 10}} returnKeyType={ 'done' } />
              </Item>

              <ListItem
                button
                onPress={() => this.loadFile("national_id")}>
                  <Grid>
                    <Col size={1} >
                        <Image style={{ width: 50,height: 50}} resizeMode="contain" source={fileImage}/>
                    </Col>
                    <Col size={4}>
                      <View style={{justifyContent: 'center',alignItems: 'center'}}>
                        <FontAwesomeIcon name="cloud-upload" size={25}  style={{ color: "#FF9900" }} />
                        <Text>Click to Upload</Text>
                        <Text note style={c_styles.textTheme}>National ID</Text>
                      </View>
                    </Col>
                  </Grid>
                  <Right>
                    <FontAwesomeIcon name="refresh" size={25} style={{ color: "#FF9900" }} />
                  </Right>
                </ListItem>

                <ListItem
                  button
                  onPress={() => this.loadFile("good_conduct")}>
                    <Grid>
                      <Col size={1} >
                          <Image style={{ width: 50,height: 50}} resizeMode="contain" source={fileImage}/>
                      </Col>
                      <Col size={4}>
                        <View style={{justifyContent: 'center',alignItems: 'center'}}>
                          <FontAwesomeIcon name="cloud-upload" size={25}  style={{ color: "#FF9900" }} />
                          <Text>Click to Upload</Text>
                          <Text note style={c_styles.textTheme}>Certificate of Good Conduct</Text>
                        </View>
                      </Col>
                    </Grid>
                    <Right>
                      <FontAwesomeIcon name="refresh" size={25} style={{ color: "#FF9900" }} />
                    </Right>
                  </ListItem>
            </View>
          }
          </Form>

          <View />
          <Button block rounded style={c_styles.buttonT25}>
            <Text>Submit Documents</Text>
          </Button>

        </Content>
      </Container>
    );
  }
}

export default Register;
