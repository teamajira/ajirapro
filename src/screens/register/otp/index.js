import React, { Component } from "react";
import Icon from 'react-native-vector-icons/FontAwesome5';
import { ImageBackground, Image,Keyboard ,NetInfo,AsyncStorage } from "react-native";
import {NavigationActions} from 'react-navigation';
import {Container,Header,Title,H2,View,Content,Button,Item,Label,Input,Body,Left,Right,Form,Text,Toast} from "native-base";
import c_styles from "AjiraPro/src/screens/common_styles";
import FormData from 'FormData';
import DeviceInfo from 'react-native-device-info';
import { COLOR_ORANGE,COLOR_WHITE,HASH,API_URL,APP_KEY,APP_VERSION,API_REQUEST_OTP,API_VERIFY_OTP } from "AjiraPro/src/screens/common_scripts";
import styles from "./styles";

const banner = require("AjiraPro/assets/block.png")

let sha1 = require('sha1');
const deviceId = DeviceInfo.getUniqueID();
const osVersion = DeviceInfo.getVersion();

export default class OTP extends Component {

  constructor(props){

    super(props);

    var { params } = this.props.navigation.state;
    var otpSalt = params ? params.otpSalt : null
    uid = params ? params.uid : null

    this.state ={
      code: "",
      otpSalt: otpSalt,
      registrationError: null
    }

    console.log("Salt "+ otpSalt+ " Uid"+uid);
  }

  validation = (type) => {
    if ((type === "verify") && (this.state.code.length !== 6) ) {
      this.setState(() => ({ registrationError: "Code required and MUST be 6 digits" }));
    } else {
      this.setState(() => ({ registrationError: null }));
    }

    if(this.state.registrationError === null){
      this.accessOTP(type);
    }else{
      Toast.show({
        text: this.state.registrationError,
        duration: 3000,
        buttonTextStyle: {color: "yellow"},
        buttonText: "Dismiss"});
    }
  }

  accessOTP = (type) => {

    let url = null;
    let hash = null;
    let rand = sha1(new Date().getTime());;
    const formData = new FormData();
    const navigateAction = NavigationActions.navigate({
      routeName: 'Documents'
    })

    if(type === "verify"){
      url = API_URL + API_VERIFY_OTP;
      hash = HASH(API_VERIFY_OTP, rand)
      formData.append('code', this.state.code);
      formData.append('otpSalt', this.state.otpSalt);
    }else{
      url = API_URL + API_REQUEST_OTP;
      hash = HASH(API_REQUEST_OTP, rand)
      formData.append('uid', uid);
    }

    formData.append('hash', hash);
    formData.append('rand', rand);
    formData.append('uuid', deviceId);
    formData.append('osVersion', osVersion);

    let postData = {
      method: 'POST',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'multipart/form-data'
      },
      body: formData
    }

    console.log("OTP postData:" + JSON.stringify(postData));

    NetInfo.isConnected.fetch().then((isConnected) => {
      if (isConnected) {
        fetch(url, postData)
          .then(response => {
            return response.json()
          })
          .then((responseJson) => {

            console.log("OTP response:" + JSON.stringify(responseJson));
            console.log("Message "+responseJson.settings.message);

            Toast.show({
              text: responseJson.settings.message,
              duration: 3000,
              buttonTextStyle: {color: "yellow"},
              buttonText: "Dismiss"});

            Keyboard.dismiss();
            if (responseJson.settings.success && type === "verify"){
                AsyncStorage.setItem("loginToken", responseJson.data.token);
                this.props.navigation.dispatch(navigateAction);
            }else if (responseJson.settings.success && type === "resend"){
                this.setState({
                  otpSalt: responseJson.data.optSalt
                })
            }

          })
          .catch((error) => {
            if (error == 'TypeError: Network request failed'){
              Toast.show({
                text: 'Unable to complete request, contact administrator',
                duration: 3000,
                buttonTextStyle: {color: "yellow"},
                buttonText: "Dismiss"});
            }
          })
      } else {
        alert("Kindly check if the device is connected to stable cellular data connection or WiFi.");
      }
    });

    this.setState({code: "" })
  }

  render() {
    return (
      <Container style={c_styles.container}>
        <Header androidStatusBarColor="#9CDEE6" hasSegment style={c_styles.bgColor}>
          <Left>
            <Button transparent onPress={() => this.props.navigation.goBack()}>
              <Icon name="arrow-left" color={COLOR_WHITE} size={15} />
            </Button>
          </Left>
          <Body>
            <Title>OTP</Title>
          </Body>
          <Right />
        </Header>

        <Content>
          <ImageBackground source={banner} style={styles.cover} >
            <View style={styles.logoContainer}>
              <Icon name="envelope-open-text" color={COLOR_WHITE} size={100} style={styles.logo} />
            </View>
          </ImageBackground>
          <H2 style={[c_styles.alignCenter,c_styles.textTheme]}>One Time Password</H2>
          <Text style={{marginTop:15,marginLeft:15}}>{!!this.state.registrationError && (<Text style={{ color: "red" }}>{this.state.registrationError}</Text>)}</Text>
          <Text style={[c_styles.alignCenter,c_styles.colorGrey]}>Confirm Phone Number By</Text>
          <Text style={[c_styles.alignCenter,c_styles.colorGrey]}>Entering the SMS code sent to you</Text>
          <Form style={c_styles.formSpacing}>
            <Item floatingLabel>
              <Label>Code</Label>
              <Input style={{marginTop:10}}
                      keyboardType = "numeric"
                      returnKeyType={ 'done' }
                      value={ this.state.code }
                      onChangeText={code => this.setState({code})}/>
            </Item>
          </Form>

          <View style={c_styles.marginSide15}>
            <Button block rounded
                    style={[c_styles.alignCenter,c_styles.bgColor]}
                    onPress={() => this.validation("verify")}>
              <Text>Send code</Text>
            </Button>
            <Button block rounded
                    style={[c_styles.alignCenter,c_styles.colorOrange]}
                    onPress={() => this.validation("resend")}>
              <Text>ReSend OTP</Text>
            </Button>
          </View>

        </Content>
      </Container>
    );
  }
}
