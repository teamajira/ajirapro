import React, { Component } from "react";
import {View,Keyboard,NetInfo} from "react-native";
import {NavigationActions} from 'react-navigation';
import {Container,Header,Title,Content,Segment,Button,Item,Label,Input,Body,Left,Right,Icon,Form,Text, Toast} from "native-base";
import FormData from 'FormData';
import DeviceInfo from 'react-native-device-info';
import c_styles from "../common_styles";

import { HASH,API_URL,APP_KEY,APP_VERSION,API_REGISTER_INDIVIDUAL,API_REGISTER_BUSINESS } from "../common_scripts";

let sha1 = require('sha1');
const deviceId = DeviceInfo.getUniqueID();
const osVersion = DeviceInfo.getVersion();

export default class Register extends Component {
  constructor(props) {
    super(props);
    this.state = {
      seg: 1,
      businessName:"",
      firstName:"",
      lastName:"",
      email: "",
      phone:"",
      password: "",
      hidden:true,
      iconName : "md-eye-off",
      jsonData:""
    };
  }

  changeSegment(segment){
    this.setState({
      seg: segment,
      businessName:"",
      firstName:"",
      lastName:"",
      email: "",
      phone:"",
      password: ""
    })
  }

  validation = (account_type) => {
    let error = null;
    if (this.state.businessName.trim() === "" && (account_type === "business")) {
      error = "Business name required.";
    }else if (this.state.firstName.trim() === "") {
      error = "First name required.";
    }else if (this.state.lastName.trim() === "") {
      error ="Last name required.";
    }else if (this.state.phone.trim() === "") {
      error = "Phone Number required.";
    }else if (this.state.email.trim() === "") {
      error = "Email required.";
    }else if (this.state.password.trim() === "") {
      error = "Password required.";
    }else if (this.password_validate(this.state.password.trim()) !== true) {
      error = this.password_validate(this.state.password.trim());
    } else {
      error = null;
    }

    if(error === null){
      this.register(account_type);
    }else{
      Toast.show({
        text: error,
        duration: 3000,
        buttonTextStyle: {color: "yellow"},
        buttonText: "Dismiss"});
    }
  }

  password_validate(pass) {
    if (pass.length < 8) {
      return("Password is too short");
    } else if (pass.length > 32) {
      return("Password is too long");
    } else if (pass.search(/\d/) == -1) {
      return("Password requires at least one number");
    } else if (pass.search(/[a-zA-Z]/) == -1) {
      return("Password requires at least one letter");
    } else if (pass.search(/[\!\@\#\$\%\^\&\*\(\)\_\+]/) == -1) {
      return("Password requires a Special character");
    }

    return true;
  }

  onFocus = () => {
        this.setState({
        backgroundColor: "#fff7eb"
        })
   }

    onBlur = () => {
        this.setState({
            backgroundColor: "transparent"
        })
    }

  hidePassword = () =>{
      this.setState({ hidden: !this.state.hidden})
      if(this.state.hidden){
          this.setState({ iconName: "md-eye"})
      }else{
          this.setState({ iconName: "md-eye-off"})
      }
  }

  register = (account_type) => {

    let url = null;
    let hash = null;
    let rand = sha1(new Date().getTime());
    const formData = new FormData();


    if(account_type === "individual"){
      url = API_URL + API_REGISTER_INDIVIDUAL;
      hash = HASH(API_REGISTER_INDIVIDUAL, rand)
    }else{
      url = API_URL + API_REGISTER_BUSINESS;
      hash = HASH(API_REGISTER_BUSINESS, rand)
      formData.append('businessName', this.state.businessName);
    }

    formData.append('hash', hash);
    formData.append('rand', rand);
    formData.append('uuid', deviceId);
    formData.append('osVersion', osVersion);
    formData.append('businessName', this.state.businessName);
    formData.append('firstName', this.state.firstName);
    formData.append('lastName', this.state.lastName);
    formData.append('phone', this.state.phone);
    formData.append('email', this.state.email);
    formData.append('email', this.state.email);
    formData.append('password', this.state.password);

    let postData = {
      method: 'POST',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'multipart/form-data'
      },
      body: formData
    }

    console.log("Register postData:" + JSON.stringify(postData));

    NetInfo.isConnected.fetch().then((isConnected) => {
      if (isConnected) {
        fetch(url, postData)
          .then(response => {
            return response.json()
          })
          .then((responseJson) => {

            console.log("Register response:" + JSON.stringify(responseJson));
            this.setState({
              //jsonData: JSON.stringify(responseJson)
            })

            Keyboard.dismiss();
            if (responseJson.settings.success) {
                  const navigateAction = NavigationActions.navigate({
                    routeName: 'OTP',
                    params:{
                      otpSalt: responseJson.data.otpSalt,
                      uid:responseJson.data.uid
                    }
                  })
                  this.props.navigation.dispatch(navigateAction);
                }

            Toast.show({
              text: responseJson.settings.message,
              duration: 3000,
              buttonTextStyle: {color: "yellow"},
              buttonText: "Dismiss"});

          })
          .catch((error) => {
            if (error == 'TypeError: Network request failed'){
              Toast.show({
                text: 'Unable to complete request, contact administrator',
                duration: 3000,
                buttonTextStyle: {color: "yellow"},
                buttonText: "Dismiss"});
            }
          })
      } else {
        alert("Kindly check if the device is connected to stable cellular data connection or WiFi.");
      }
    });
  }

  render() {
    return (
      <Container style={c_styles.container}>
        <Header androidStatusBarColor="#9CDEE6" hasSegment style={c_styles.bgColor}>
          <Left>
            <Button transparent onPress={() => this.props.navigation.goBack()}>
              <Icon name="arrow-back" />
            </Button>
          </Left>
          <Body>
            <Title>Registration</Title>
          </Body>
          <Right />
        </Header>
        <Segment style={c_styles.bgColor}>
          <Button
            first
            style={c_styles.roundLeft}
            active={this.state.seg === 1 ? true : false}
            onPress={() => this.changeSegment(1)}>
            <Text>Individual</Text>
          </Button>
          <Button
            style={c_styles.roundRight}
            active={this.state.seg === 2 ? true : false}
            onPress={() => this.changeSegment(2)}>
            <Text>Business</Text>
          </Button>
        </Segment>

        <Content>
          {/* Individual */}
          {
              this.state.seg === 1 &&
            <View>
              <Form style={c_styles.formSpacing}>
                <Item floatingLabel>
                  <Label>First Name</Label>
                  <Input style={{marginTop: 10}}
                          keyboardType = "default"
                          returnKeyType={ 'next' }
                          onChangeText={firstName => this.setState({firstName})}/>
                </Item>
                <Item floatingLabel>
                  <Label>Last Name</Label>
                  <Input style={{marginTop: 10}}
                          keyboardType = "default"
                          returnKeyType={ 'next' }
                          onChangeText={lastName => this.setState({lastName})} />
                </Item>
                <Item floatingLabel>
                  <Label>Phone Number</Label>
                  <Input style={{marginTop: 10}}
                          keyboardType = "numeric"
                          returnKeyType={ 'next' }
                          onChangeText={phone => this.setState({phone})} />
                </Item>
                <Item floatingLabel>
                  <Label>Email</Label>
                  <Input style={{marginTop: 10}}
                          keyboardType = "email-address"
                          returnKeyType={ 'next' }
                          onChangeText={email => this.setState({email})} />
                </Item>
                <Item floatingLabel>
                  <Label>Password</Label>
                  <Input
                    style={{marginTop: 10}}
                    returnKeyType={ 'done' }
                    onBlur={ () => this.onBlur() }
                    onFocus={ () => this.onFocus() }
                    secureTextEntry={this.state.hidden}
                    onChangeText={password => this.setState({password})}/>
                    <Icon name={this.state.iconName} onPress={() => this.hidePassword()} style={{color:"#333"}} />
                </Item>
              </Form>

              <View />
              <Button block rounded style={c_styles.buttonT25} onPress={() => this.validation("individual")}>
                <Text>Register</Text>
              </Button>
            </View>
          }

          {/*Business*/}
          {
            this.state.seg === 2 &&
            <View>
              <Form style={c_styles.formSpacing}>
                <Item floatingLabel>
                  <Label>Business Name</Label>
                  <Input style={{marginTop: 10}}
                          keyboardType = "default"
                          returnKeyType={ 'next' }
                          onChangeText={businessName => this.setState({businessName})}/>
                </Item>
                <Item floatingLabel>
                  <Label>First Name</Label>
                  <Input style={{marginTop: 10}}
                          keyboardType = "default"
                          returnKeyType={ 'next' }
                          onChangeText={firstName => this.setState({firstName})}/>
                </Item>
                <Item floatingLabel>
                  <Label>Last Name</Label>
                  <Input style={{marginTop: 10}}
                          keyboardType = "default"
                          returnKeyType={ 'next' }
                          onChangeText={lastName => this.setState({lastName})} />
                </Item>
                <Item floatingLabel>
                  <Label>Phone Number</Label>
                  <Input style={{marginTop: 10}}
                          keyboardType = "numeric"
                          returnKeyType={ 'next' }
                          onChangeText={phone => this.setState({phone})} />
                </Item>
                <Item floatingLabel>
                  <Label>Email</Label>
                  <Input style={{marginTop: 10}}
                          keyboardType = "email-address"
                          returnKeyType={ 'next' }
                          onChangeText={email => this.setState({email})} />
                </Item>
                <Item floatingLabel>
                  <Label>Password</Label>
                  <Input
                    style={{marginTop: 10}}
                    returnKeyType={ 'done' }
                    onBlur={ () => this.onBlur() }
                    onFocus={ () => this.onFocus() }
                    secureTextEntry={this.state.hidden}
                    onChangeText={password => this.setState({password})}/>
                    <Icon name={this.state.iconName} onPress={() => this.hidePassword()} style={{color:"#333"}}  />
                </Item>
              </Form>

              <View />
              <Button block rounded style={c_styles.buttonT25} onPress={() => this.validation("business")}>
                <Text>Register</Text>
              </Button>
            </View>
          }



          <View style={{ margin: 10 }} >
            <Text style={{textAlign: "center"}}>Already have an account ?
              <Text
              style={[c_styles.underline]}
              onPress={() => this.props.navigation.navigate('Login')}> Log In
               </Text>
            </Text>
          </View>

        </Content>
      </Container>
    );
  }
}
